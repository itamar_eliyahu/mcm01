onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {color bar gen}
add wave -noupdate /Parallel2CSI2_tb/u_colorbar_gen/rstn
add wave -noupdate /Parallel2CSI2_tb/u_colorbar_gen/clk
add wave -noupdate -color Magenta /Parallel2CSI2_tb/u_colorbar_gen/fv
add wave -noupdate -color Yellow /Parallel2CSI2_tb/u_colorbar_gen/lv
add wave -noupdate -color Cyan -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/data
add wave -noupdate -color Orange -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/pixcnt
add wave -noupdate /Parallel2CSI2_tb/u_colorbar_gen/linecnt
add wave -noupdate /Parallel2CSI2_tb/u_colorbar_gen/fv_cnt
add wave -noupdate /Parallel2CSI2_tb/u_colorbar_gen/color_cntr
add wave -noupdate /Parallel2CSI2_tb/u_colorbar_gen/q_fv
add wave -noupdate /Parallel2CSI2_tb/u_colorbar_gen/rstn_cnt
add wave -noupdate /Parallel2CSI2_tb/u_colorbar_gen/vsync
add wave -noupdate /Parallel2CSI2_tb/u_colorbar_gen/hsync
add wave -noupdate -divider Parameters
add wave -noupdate -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/h_active
add wave -noupdate -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/h_total
add wave -noupdate -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/v_active
add wave -noupdate -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/v_total
add wave -noupdate -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/H_FRONT_PORCH
add wave -noupdate -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/H_SYNCH
add wave -noupdate -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/H_BACK_PORCH
add wave -noupdate -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/V_FRONT_PORCH
add wave -noupdate -radix unsigned /Parallel2CSI2_tb/u_colorbar_gen/V_SYNCH
add wave -noupdate /Parallel2CSI2_tb/u_colorbar_gen/mode
add wave -noupdate -divider {Generator outputs}
add wave -noupdate /Parallel2CSI2_tb/u_top/PIXCLK
add wave -noupdate /Parallel2CSI2_tb/u_top/FV
add wave -noupdate /Parallel2CSI2_tb/u_top/LV
add wave -noupdate -radix unsigned /Parallel2CSI2_tb/u_top/PIXDATA
add wave -noupdate -divider {byte packetizer}
add wave -noupdate -divider parameters
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/crc16
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/dt
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/lane_width
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/word_width
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/version
add wave -noupdate -divider <NULL>
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/reset_n
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/PIXCLK
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/FV
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/LV
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/PIXDATA
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/VC
add wave -noupdate -radix hexadecimal /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/WC
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/byte_clk
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/hs_en
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/byte_D3
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/byte_D2
add wave -noupdate -color Cyan /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/byte_D0
add wave -noupdate -color Cyan -radix hexadecimal /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/byte_D1
add wave -noupdate -divider {packet header}
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_packetheader/short_en
add wave -noupdate -color Orange /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_packetheader/long_en
add wave -noupdate -color Yellow /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_packetheader/byte_data
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_packetheader/chksum_rdy
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_packetheader/chksum
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_packetheader/EoTp
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_packetheader/wc
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_packetheader/vc
add wave -noupdate -divider {Parallel to byte}
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/PIXCLK
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/LV
add wave -noupdate -color Magenta /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/PIXDATA
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/byte_en
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/byte_data
add wave -noupdate -divider {MIPI output}
add wave -noupdate /Parallel2CSI2_tb/u_top/u_DPHY_TX_INST/reset_n
add wave -noupdate /Parallel2CSI2_tb/u_top/u_DPHY_TX_INST/hsxx_clk_en
add wave -noupdate /Parallel2CSI2_tb/u_top/u_DPHY_TX_INST/CLKOP
add wave -noupdate /Parallel2CSI2_tb/u_top/u_DPHY_TX_INST/CLKOS
add wave -noupdate /Parallel2CSI2_tb/u_top/u_DPHY_TX_INST/hs_clk_en
add wave -noupdate /Parallel2CSI2_tb/u_top/u_DPHY_TX_INST/hs_data_en
add wave -noupdate /Parallel2CSI2_tb/u_top/u_DPHY_TX_INST/byte_D1
add wave -noupdate /Parallel2CSI2_tb/u_top/u_DPHY_TX_INST/byte_D0
add wave -noupdate -color Coral /Parallel2CSI2_tb/u_top/DCK
add wave -noupdate -color Coral /Parallel2CSI2_tb/u_top/D1
add wave -noupdate -color Coral /Parallel2CSI2_tb/u_top/D0
add wave -noupdate -divider <NULL>
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/data_type
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/FV_end
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/byte_en
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/byte_data
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/FV_start
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/reset_n
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/FV
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/byte_clk
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/PIXDATA
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/LV
add wave -noupdate /Parallel2CSI2_tb/u_top/u_BYTE_PACKETIZER/u_parallel2byte/PIXCLK
add wave -noupdate /Parallel2CSI2_tb/u_top/byte_clk
add wave -noupdate /Parallel2CSI2_tb/u_top/CLKOP
add wave -noupdate /Parallel2CSI2_tb/u_top/CLKOS
add wave -noupdate /Parallel2CSI2_tb/u_top/w_pixclk
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2181104000000 fs} 0} {{Cursor 2} {2181136000000 fs} 0}
quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 92
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2181083927300 fs} {2181200872700 fs}
