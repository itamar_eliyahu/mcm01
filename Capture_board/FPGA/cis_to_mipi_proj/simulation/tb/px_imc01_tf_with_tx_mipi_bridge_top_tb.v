//~ `New testbench
`timescale  1ns / 10ps
 //`define original_RX_bridge
 `define INCLUDE_FRAME_GRABBER

module px_imc01_tf_with_tx_mipi_bridge_top_tb;
   GSR GSR_INST(.GSR(1'b1));
   PUR PUR_INST(.PUR(1'b1));	
// px_imc01_tf_with_tx_mipi_bridge_top Parameters
parameter PERIOD          = 20      ;
parameter RX_RESET_PERIOD          = 470      ;

parameter CIS_size        = 1440    ;
parameter LED_colors      = 8       ;
parameter M4_sensors      = 38      ;
parameter DVP_pixels      = 2918    ;
parameter CIS_CHIP_PIXEL  = 288     ;

parameter VC              = 0       ;
parameter WC              = 16'd3328;
parameter word_width      = 8       ;
parameter DT              = 6'h2A   ;
parameter testmode        = 0       ;
parameter crc16           = 1       ;
parameter reserved        = 0       ;
parameter lane_width      = 2       ;
parameter format          = "RAW8"  ;
// px_imc01_tf_with_tx_mipi_bridge_top Inputs
reg   OSC_50MHz                            = 0 ;
reg   [15:0]  CIS1_D                       = 0 ;
reg   CIS1_VS                              = 0 ;
reg   CIS1_HS                              = 0 ;
reg   CIS1_PCLK                            = 0 ;
reg   oCIS1_RLED                           = 0 ;
reg   oCIS1_GLED                           = 0 ;
reg   oCIS1_BLED                           = 0 ;
reg   oCIS1_IRLED                          = 0 ;
reg   oCIS1_UVLED                          = 0 ;
reg   [15:0]  CIS2_D                       = 0 ;
reg   CIS2_VS                              = 0 ;
reg   CIS2_HS                              = 0 ;
reg   CIS2_PCLK                            = 0 ;
reg   oCIS2_RLED                           = 0 ;
reg   oCIS2_GLED                           = 0 ;
reg   oCIS2_BLED                           = 0 ;
reg   oCIS2_IRLED                          = 0 ;
reg   oCIS2_UVLED                          = 0 ;
reg   SPI_SI                               = 0 ;
reg   SPI_SCK                              = 0 ;
reg   SPI_CSSN                             = 0 ;
reg   D0_p                                 = 0 ;
reg   D0_n                                 = 0 ;
reg   D1_p                                 = 0 ;
reg   D1_n                                 = 0 ;
reg   DCK_p                                = 0 ;
reg   DCK_n                                = 0 ;
reg   reset_n                              = 0 ;
reg   rx_reset_n                              = 0 ;


// px_imc01_tf_with_tx_mipi_bridge_top Outputs
wire  [1:0]  oCLK_V72;
wire  oLED_R_T;
wire  oLED_G_T;
wire  oLED_B_T;
wire  oLED_IR_T;
wire  SPI_SO;
wire  TP19;
wire  TP42;
wire  [2:0]  oLED;
wire  MIPI_DCK;
wire  D3;
wire  MIPI_D0;
wire  MIPI_D1;
wire  D2;
wire [7:0] ParallelVideo,pixdata;
wire LineValid,FieldValid,RX_FVAL,RX_LVAL;


initial
begin
    forever #(PERIOD/2)  OSC_50MHz=~OSC_50MHz;
end

initial
begin
    #(PERIOD*2) reset_n  =  1;
end

initial
begin
    #(RX_RESET_PERIOD*2) rx_reset_n  =  1;
end


px_imc01_tf_with_tx_mipi_bridge_top #(
    .CIS_size       ( CIS_size       ),
    .LED_colors     ( LED_colors     ),
    .M4_sensors     ( M4_sensors     ),
    .DVP_pixels     ( DVP_pixels     ),
    .CIS_CHIP_PIXEL ( CIS_CHIP_PIXEL ),
    .VC             ( VC             ),
    .WC             ( WC             ),
    .word_width     ( word_width     ),
    .DT             ( DT             ),
    .testmode       ( testmode       ),
    .crc16          ( crc16          ),
    .reserved       ( reserved       ),
    .lane_width     ( lane_width     ))
 u_px_imc01_tf_with_tx_mipi_bridge_top (
    .OSC_50MHz(OSC_50MHz),
    .CIS1_D(CIS1_D[15:0]),
    .CIS1_VS(CIS1_VS),
    .CIS1_HS(CIS1_HS),
    .CIS1_PCLK(CIS1_PCLK),
    .oCIS1_RLED(oCIS1_RLED),
    .oCIS1_GLED(oCIS1_GLED),
    .oCIS1_BLED(oCIS1_BLED),
    .oCIS1_IRLED(oCIS1_IRLED),
    .oCIS1_UVLED(oCIS1_UVLED),
    .CIS2_D(CIS2_D[15:0]),
    .CIS2_VS(CIS2_VS),
    .CIS2_HS(CIS2_HS),
    .CIS2_PCLK(CIS2_PCLK),
    .oCIS2_RLED(oCIS2_RLED),
    .oCIS2_GLED(oCIS2_GLED),
    .oCIS2_BLED(oCIS2_BLED),
    .oCIS2_IRLED(oCIS2_IRLED),
    .oCIS2_UVLED(oCIS2_UVLED),
    .SPI_SI(SPI_SI),
    .SPI_SCK(SPI_SCK),
    .SPI_CSSN( SPI_CSSN),
    .D0_p( D0_p),
    .D0_n( D0_n),
    .D1_p( D1_p),
    .D1_n( D1_n),
    .DCK_p( DCK_p),
    .DCK_n( DCK_n),
    .reset_n( reset_n),

    .oCLK_V72( oCLK_V72[1:0]  ),
    .oLED_R_T( oLED_R_T),
    .oLED_G_T( oLED_G_T),
    .oLED_B_T( oLED_B_T),
    .oLED_IR_T( oLED_IR_T),
    .SPI_SO( SPI_SO),
    .TP19( TP19),
    .TP42( TP42),
    .oLED( oLED[2:0]  ),
    .DCK( MIPI_DCK),
    .D1( MIPI_D1),
    .D0( MIPI_D0),
    .ParallelVideo(ParallelVideo), 
    .LineValid(LineValid), 
    .FieldValid(FieldValid)
                                                                                                     
);





parameter InputFrameWidth =3328;
parameter InputFrameHeight =936;

`ifdef TxEmulator
   /************* Camera Link Tx Video Simulator ***********************************/
   defparam CameraLinkTx.FrameWidth	   = InputFrameWidth;
   defparam CameraLinkTx.FrameHeight	= InputFrameHeight;
      CameraLink8bitTxEmulator CameraLinkTx(
         .clk(OSC_50MHz),
         .Reset(reset_n),
         .PixClk(OSC_50MHz),
         .ParallelVideoOut(ParallelVideo), 
         .LineValidOut(LineValid), 
         .FieldValidOut(FieldValid)
      );

 `else

      defparam PatternGenerator.FrameWidth	   = InputFrameWidth;
      defparam PatternGenerator.FrameHeight	= InputFrameHeight;
      CameraLink8bitPatternGenerator PatternGenerator(
         .clk(OSC_50MHz),
         .Reset(reset_n),
         .PixClk(OSC_50MHz),
         .ParallelVideoOut(ParallelVideo), 
         .LineValidOut(LineValid), 
         .FieldValidOut(FieldValid) 
      ); 
   `endif      


   parameter num_frames        = 10  ;
   parameter htotal            = 2200/lane_width ;
   parameter hactive           = 1920/lane_width ;
   parameter htotal_8bit       = (word_width*htotal) /8 ; 
   parameter hactive_8bit      = (word_width*hactive)/8 ;
   parameter vtotal            = 1125;
   parameter vactive           = 1080;   

   reg                      clk,rstn           ;
   reg                      clkx2          ; 
   reg                      clkx4          ;
   reg                      clkx8          ;
   reg                      clkx16         ;
reg  [15:0] data_in;
   reg  [7:0]  data0, data1, d0;
   reg  dout, dout1;
   reg  [2:0] data_cntr; 
   reg  hactive_flag;
   
   initial begin                       
      rstn       = 1'b0;          
      clk        = 1'b0; clkx2       = 1'b0; clkx4       = 1'b0; clkx8       = 1'b0;  clkx16       = 1'b0;
      data_in    = 'b0; d0 =1'b0; 
      data_cntr  = 'b0;
	  hactive_flag = 'b0;
      #1200 rstn = 1'b1; 
      #1200; 
       repeat (num_frames) begin  
	       //FS - Frame Start
		   repeat (1) begin
            @(negedge clkx2) data_in = 'h1D1D; // Leader Sequence = 00011101,
            @(negedge clkx2) data_in = 'h0000;//'h0000; // Data ID = {VC = 1, Frame Start Code = 0},
            @(negedge clkx2) data_in = 'h00AA; // short packet data =channel 0, ECC=10101010
			  repeat(htotal_8bit-3)
			      @(negedge clkx2) data_in = 'h0000; 
	       end
		   //Active region
		   repeat (vactive) begin
           //Packet Header 
            @(negedge clkx2) data_in = 'h1D1D; // Leader Sequence = 00011101,
            @(negedge clkx2) data_in = 'h0254; // Data ID = {VC = 0, Device Type = 0x1E = YUV 422 8 bit},
            @(negedge clkx2) begin data_in = 'hB860; hactive_flag = 1; end// ECC=1D
           //line data 1,2,3,...
            //@(negedge clkx2) begin data_in = 'h0101;  end
            repeat(hactive_8bit)
               @(negedge clkx2) begin data_in = {data0, data1}; end  
        //Packet Footer
        @(negedge clkx2) begin data_in = 'hCEEC; hactive_flag = 0; end	//checksum = 0xCEEC 
        @(negedge clkx2) data_in = 'hCEEC;	//checksum = 0xCEEC	
			  //Blanking
			  repeat(htotal_8bit-hactive_8bit-3-2) //blanking = total - active - PH - PF
			      @(negedge clkx2) data_in = 0;	
	       end
		   //FE - Frame End
		   repeat (1) begin
            @(negedge clkx2) data_in = 'h1D1D; // Leader Sequence = 00011101,
	          @(negedge clkx2) data_in = 'h0001; // Data ID = {VC = 1, Frame End Code = 1},
		        @(negedge clkx2) data_in = 'h00AA; // Short Packet Data = channel 0, ECC=10101010		 
			  repeat(htotal_8bit-3)
			      @(negedge clkx2) data_in = 'h0000; 
           end 
   		   //vertical blanking 
		   repeat (vtotal-vactive-1-1) begin  //blanking = total - active - FS - FE	
			   repeat(htotal_8bit)
            @(negedge clkx2) data_in = 'h0000;
		   end
       end
   end
   
   parameter periode_16    = 1 ;  
   parameter periode_8     = 2 ;
   parameter periode_4     = 4 ;
   parameter periode_2     = 8 ; 
   parameter periode_1     = 16;  
   always clk    = #periode_1  ~clk;                                      
   always clkx2  = #periode_2  ~clkx2; 
   always clkx4  = #periode_4  ~clkx4;            
   always clkx8  = #periode_8  ~clkx8;
   always clkx16 = #periode_16 ~clkx16;
   
   always @(posedge clkx4 or negedge rstn) begin
       if(~hactive_flag)   begin
		 data0 <= 0;
		 data1 <= 0; 
		 d0    <= 0;
		 data_cntr <= 0;
	   end
	   else	 begin
         data0     <= {d0[0],d0[1],d0[2],d0[3],d0[4],d0[5],d0[6],d0[7]};
		 data1     <= data0;
		 d0        <= d0 + (data_cntr==3); 
		 data_cntr <= (data_cntr<3) ? data_cntr+1 : 0;
	   end
   end
	   
	   
integer i;
   always @(posedge clkx16 or negedge rstn) begin
       if(~rstn)
          i <= 7;
       else if (i>0)
          i <= i-1;
       else
		  i <=7;
   end	
   always @(posedge clkx16 or negedge rstn) begin
       if(~rstn) begin
		 dout  <= 0;
		 dout1 <= 0;	
	   end	 
	   else	begin
         dout  <= data_in[i];  	
		 dout1 <= data_in[i+8];	 
	   end
   end







  `ifdef original_RX_bridge

   MIPI_CSI2_Serial2Parallel_Bridge  #(
      .bus_width         (word_width    ),
      .lane_width        (lane_width    ),
	  .format            (format        ))

   mipi_rx_bridge(                 
      .rstn              ( rstn ),
      .DCK               ( clkx8),   // serial clock
      .CH0               ( dout ),
      .CH1               ( dout1 ),
      .CH2               ( dout ),
      .CH3               ( dout ),   // LVDS DDR input data
      .pixclk_adj        (      ),   
      .pixdata           (      ),   
      .fv                ( RX_FVAL),   
      .lv                ( RX_LVAL),
	  .sensor_clk  		 ( clkx8)
   );	


`else

  wire mipi_dclk_non_z = (MIPI_DCK === 1'bz) ? 1'b0 : MIPI_DCK;
  wire mipi_d0_non_z = (MIPI_D0 === 1'bz) | (MIPI_D0 === 1'bx)  ? 1'b0 : MIPI_D0;
  wire mipi_d1_non_z = (MIPI_D1 === 1'bz) | (MIPI_D1 === 1'bx) ? 1'b0 : MIPI_D1;
  wire mipi_out_clk;
   MIPI_CSI2_Serial2Parallel_Bridge  #(
      .bus_width         (word_width),
      .lane_width        (lane_width),
	  .format            (format),
      .line_length_detect(1'b0),
      .line_length       (WC))   
   mipi_rx_bridge(                 
      .rstn              ( rx_reset_n ),
      .DCK               ( mipi_dclk_non_z),   // serial clock
      .CH0               ( mipi_d0_non_z ),
      .CH1               ( mipi_d1_non_z ),
      .CH2               ( mipi_d0_non_z ),
      .CH3               ( mipi_d0_non_z ),   // LVDS DDR input data
      .pixclk_adj        ( mipi_out_clk     ),   
      .pixdata           (  pixdata[7:0]    ),   
      .fv                ( RX_FVAL    ),   
      .lv                ( RX_LVAL     ),
	  .sensor_clk  		 ( mipi_dclk_non_z)
   );	

`endif

`ifdef INCLUDE_FRAME_GRABBER
/************* Camera Link Frame Grabber *******************************************/
defparam FramGrabber.FrameWidth	= InputFrameWidth;
defparam FramGrabber.FrameHeight = InputFrameHeight;
CameraLink8bitFrameGrabber FramGrabber(
		.Reset				(rx_reset_n),
		.PixClk				(mipi_out_clk),
		.ParallelVideoIn	(pixdata[7:0]),
		.LineValidIn		(RX_LVAL),  
		.FieldValidIn		(RX_FVAL)

       
      // .Reset(reset_n),
      // .PixClk(OSC_50MHz),
      // .ParallelVideoIn(ParallelVideo[7:0]), 
      // .LineValidIn(LineValid), 
      // .FieldValidIn(FieldValid)

);
`endif


endmodule

