#do C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/mipi_rx_bridge_sim_run.do
quit -sim
vlib work
cd  C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/Modelsim

#compile
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/rtl/MIPI_CSI2_Serial2Parallel_bb.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/rtl/MIPI_CSI2_Serial2Parallel_Bridge.v
 
#vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/compiler_directives.v

#TB
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/testbench/MIPI_CSI2_Serial2Parallel_Bridge_8s_2s_RAW8.vo 
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/testbench/MIPI_CSI2_Serial2Parallel_Bridge_tb_8bit_2lane.v 


 
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/IPExpress/pll_8bit_2lane.v 
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/IPExpress/deser.v 



# for Standalone MIPI TX bridge simulation
vsim -L machxo2_vlg -L pmi_work -suppress 3584  work.mipi_csi2_tb
#do C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/mipi_core_only_wave.do

#restart -f
run -all 