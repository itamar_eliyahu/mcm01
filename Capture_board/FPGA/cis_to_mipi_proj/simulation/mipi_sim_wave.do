onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {byte packetizer}
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/reset_n
add wave -noupdate -color Magenta /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/FV
add wave -noupdate -color Yellow /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/LV
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/PIXCLK
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/PIXDATA
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/dt
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/word_width
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/lane_width
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/VC
add wave -noupdate -radix unsigned /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/WC
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/byte_clk
add wave -noupdate -color Cyan /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/byte_D1
add wave -noupdate -color Cyan /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/byte_D0
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/u_BYTE_PACKETIZER/hs_en
add wave -noupdate -divider {CameraLink simulator}
add wave -noupdate -color Orange /px_imc01_tf_with_tx_mipi_bridge_top_tb/CameraLinkTx/ParallelVideoOut
add wave -noupdate -color Yellow /px_imc01_tf_with_tx_mipi_bridge_top_tb/CameraLinkTx/LineValidOut
add wave -noupdate -color Magenta /px_imc01_tf_with_tx_mipi_bridge_top_tb/CameraLinkTx/FieldValidOut
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/CameraLinkTx/clk
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/CameraLinkTx/Reset
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/CameraLinkTx/PixClk
add wave -noupdate -divider {gen 83M}
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/clk_gen_83Mhz/CLK_DIV
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/clk_gen_83Mhz/reset_n
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/clk_gen_83Mhz/clock_in
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/clk_gen_83Mhz/clock_out
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/clk_gen_83Mhz/clock_counter
add wave -noupdate -divider {Gen 12_5M}
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/clk_gen_12_5Mhz/CLK_DIV
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/clk_gen_12_5Mhz/reset_n
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/clk_gen_12_5Mhz/clock_in
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/clk_gen_12_5Mhz/clock_out
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/clk_gen_12_5Mhz/clock_counter
add wave -noupdate -divider {MIPI BRIDGE TOp}
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/reset_n
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/DCK
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/FCLK_100Mhz
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/FCLK_83Mhz
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/FCLK_12_5Mhz
add wave -noupdate -color Yellow /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/PIXCLK
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/CLKOP
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/u_px_imc01_tf_with_tx_mipi_bridge_top/u_mipi_tx_bridge_top/CLKOS
add wave -noupdate /px_imc01_tf_with_tx_mipi_bridge_top_tb/OSC_50MHz
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 3} {74629654130 fs} 0} {{Cursor 2} {14046527056910 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 92
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {77255176320 fs} {78326818240 fs}
