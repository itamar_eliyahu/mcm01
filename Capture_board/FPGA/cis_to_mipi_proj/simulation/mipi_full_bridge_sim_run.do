#do C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/mipi_full_bridge_sim_run.do
quit -sim
vdel -all -lib work
vlib work
#cd  C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/Modelsim
cd  C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/modelsim_10_5

#RX bridge
#compile
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/rtl/MIPI_CSI2_Serial2Parallel_bb.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/rtl/MIPI_CSI2_Serial2Parallel_Bridge.v


vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/testbench/MIPI_CSI2_Serial2Parallel_Bridge_tb_8bit_2lane.v 

vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/IPExpress/pll_8bit_2lane.v 
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/IPExpress/deser.v 

#top
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/top_src/compiler_directives.v


#TX bridge
#compile
#compile
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/BYTE_PACKETIZER.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/colorbar_gen.v
#vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/.v


vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/crc16_2lane_bb.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/LP_HS_dly_ctrl.v
#vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/DCS_ROM.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/DPHY_TX_INST.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/IO_Controller_TX.v

vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/LP_HS_dly_ctrl.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/oDDRx4.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/packetheader_bb.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/parallel2byte_bb.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/top_src/clk_gen.v

vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/testbench/MIPI_CSI2_Serial2Parallel_Bridge_8s_2s_RAW8.vo 
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/tb/packetheader_2s.vo
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/tb/parallel2byte_8s_2s_43.vo
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/tb/crc16_2lane.vo

#vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/tb/cis_to_mipi_impl_vo.vo
#vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/tb/comb_netlist.vo


vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/IPExpress/pll_pix2byte_RAW10_2lane.v
#vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/IPExpress/pll_pix2byte_RAW8_2lane.v

vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/rtl/mipi_tx_bridge_top.v

vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/Frame_simulation/verilog/CameraLink8bitTxEmulator.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/Frame_simulation/verilog/CameraLink8bitFrameGrabber.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/Frame_simulation/verilog/CameraLink8bitPatternGenerator.v

# for Standalone MIPI TX bridge simulation
#vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/mipi_bridge_src/tb/Parallel2CSI2_tb_2lane.v

#CIS to DVP
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/px_imc01_src/dvp_output.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/px_imc01_src/counter.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/px_imc01_src/edge_detect.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/px_imc01_src/ht82v72_m4.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/px_imc01_src/px_imc01_tf_top.v

vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/IPs/ipexpress/PLL_120_12.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/IPs/ipexpress/LT_FIFO.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/IPs/ipexpress/LT_BRAM.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/IPs/ipexpress/LT_RAM_DP.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/IPs/ipexpress/LT_FIFO_SPI.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/IPs/ipexpress/LT_COUNTER_16.v

vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/verilog/top_src/px_imc01_tf_with_tx_mipi_bridge_top.v

vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/tb/px_imc01_tf_with_tx_mipi_bridge_top_tb.v
vlog C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/MIPI_CSI2_Serial2Parallel_Bridge_8Bit_2lane_RAW8_V4/testbench/MIPI_CSI2_Serial2Parallel_Bridge_tb_8bit_2lane.v 





# for Standalone MIPI TX bridge simulation
#vsim -L machxo2_vlg -L pmi_work -suppress 3584 work.Parallel2CSI2_tb
#do C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/mipi_core_only_wave.do

#full chip simulation
vsim -L machxo2_vlg -L pmi_work -suppress 3584 work.px_imc01_tf_with_tx_mipi_bridge_top_tb
do C:/Projects/MCM01/mcm01/Capture_board/FPGA/cis_to_mipi_proj/simulation/mipi_full_bridge_wave.do
#force -freeze sim:/px_imc01_tf_with_tx_mipi_bridge_top_tb/mipi_rx_bridge/DCK 1 2500000, 0 {7500000 fs} -r 10000000

#restart -f
run 210ms 