
module FrameSimulation
 (
  input  clk,
  input	 Reset,
  input  PixClk

);

wire [7:0] ParallelVideo;
wire LineValid,FieldValid;

wire [7:0] ScaleParallelVideoOut;
wire ScaleLineValidOut,ScaleFieldValidOut;

parameter InputFrameWidth =1920;
parameter InputFrameHeight =1080;

/************* Camera Link Tx Video Simulator ***********************************/
defparam CameraLinkTx.FrameWidth	   = InputFrameWidth;
defparam CameraLinkTx.FrameHeight	   = InputFrameHeight;
defparam CameraLinkTx.ExposureTime 	   = 20;
defparam CameraLinkTx.FirsetLinePause  = 10;
defparam CameraLinkTx.LastLinePause	   = 10;
defparam CameraLinkTx.LinePause    	   = 10;
defparam CameraLinkTx.CPRE         	   = 10;//Constant delay between end of exposure time and beginning of read-out
defparam CameraLinkTx.ExposureReset    = 10;//Constant delay between end of read-out and earliest begin of a new exposure.
  CameraLink8bitTxEmulator CameraLinkTx(
  .clk(clk),
  .Reset(Reset),
  .PixClk(PixClk),
  .ParallelVideoOut(ParallelVideo), 
  .LineValidOut(LineValid), 
  .FieldValidOut(FieldValid)
);
/***********************ScaleUpDown********************************************************
 ScaleUpDown ImageScaling
 (
	.VideoIn(ParallelVideo), //26 bit
	.LineValidIn(LineValid), 
	.FieldValidIn(FieldValid), 
	.PixClk(PixClk),  
	.VideoOut(ScaleParallelVideoOut),  
	.LineValidOut(ScaleLineValidOut), 
	.FieldValidOut(ScaleFieldValidOut),  


	.Clock(clk),  
	.Reset(Reset)
);
*/
/************* Camera Link Matlab Frame Grabber *********************************/
defparam FramGrabber.FrameWidth	   = InputFrameWidth;
defparam FramGrabber.FrameHeight   = InputFrameHeight;
CameraLink8bitMatlabFrameGrabber FramGrabber(
  .clk(clk),
  .Reset(Reset),
  .PixClk(PixClk),
  .ParallelVideoIn(/*ScaleParallelVideoOut*/ParallelVideo),
  .LineValidIn(/*ScaleLineValidOut*/LineValid),  
  .FieldValidIn(/*ScaleFieldValidOut*/FieldValid)
);
endmodule