
//`default_nettype none

module CameraLink8bitFrameGrabber (
  input	 Reset,
  input  PixClk,
  input [7:0] ParallelVideoIn,
  input LineValidIn, 
  input FieldValidIn
);

  parameter FrameWidth	 =   1280;
  parameter FrameHeight	 =   720;
    parameter image_output_raw_file_path = "C:/Projects/Python/MIPI_simulation/OutputFrameRawData_pattern.txt";

  reg [7:0] OutputFrameMem[0:(FrameWidth*FrameHeight)];//1280*720=921600
 
  reg [31:0] PixCount;
  reg [3:0]  State;
  reg [15:0] FrameCount;
  reg [15:0] LineCount ;
 
  always @ ( posedge PixClk or negedge Reset)
begin
    if ( Reset == 0 )
	begin
		PixCount[31:0]	 <=31'd0;
		State		 <=0;
		FrameCount	 <=0;
		LineCount    <=0;
    end
	
	else
	begin
	 
		case (State)
			
			0: begin
				PixCount[31:0]	 <=31'd0;
				State <= State+2;
			end	
			
			1: begin
				if (FieldValidIn)
					State <= State+1;
			  end
			  
			2: begin
				if (LineValidIn)
				   begin
					    OutputFrameMem[PixCount[31:0]] <= ParallelVideoIn;
					    PixCount[31:0] <= PixCount[31:0]+1;
				   end
				else if (~LineValidIn)
				    begin
						//PixCount[31:0] <=31'd0;
				        State <=  3;
					end	

			end	 

			3: begin 
					 if (LineCount[15:0] < FrameHeight)
					    begin
					     LineCount[15:0]<=  LineCount[15:0]+1;
						 State <=  4;
						end
					 else
					    begin
					      LineCount[15:0] <= 0;
						  $writememb (image_output_raw_file_path ,OutputFrameMem);
						State <=  5;
						end

				   
			    end

			4: begin

				if (LineValidIn)
				   begin
					    OutputFrameMem[PixCount[31:0]] <= ParallelVideoIn;
					    PixCount[31:0] <= PixCount[31:0]+1;
						State <=  2;
				   end
			end   


		endcase 
    end
end	
endmodule	
