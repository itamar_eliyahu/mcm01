
`timescale 1ns / 1ps
module FrameSimulation_tb ;

  reg    MaserClk,PixelClk,Reset; 

FrameSimulation  DUT  
( 
  .clk(MaserClk),
  .Reset(Reset),
  .PixClk(PixelClk)
);

initial
begin
	MaserClk <=0;
	PixelClk <=0;
	Reset <=0;
	#40 Reset <= 1'b1  ;  
 end
 
always begin
	#18.5  MaserClk  <= ~MaserClk  ; //27Mhz 
end 

always begin 
 #6.25  PixelClk  <= ~PixelClk  ; //80Mhz
end


endmodule
