
//`default_nettype none

module CameraLink8bitPatternGenerator (
  input  clk,
  input	 Reset,
  input  PixClk,
  output reg [7:0] ParallelVideoOut,
  output reg LineValidOut, 
  output reg FieldValidOut
);

  parameter FrameWidth	     =   1280;
  parameter FrameHeight	     =   720;
  parameter ExposureTime	 =  118;
  parameter FirsetLinePause	 = 3600;
  parameter LastLinePause	 = 1000;
  parameter LinePause	     = 272;
  parameter CPRE         	 = 10;//Constant delay between end of exposure time and beginning of read-out
  parameter ExposureReset    = 10;//Constant delay between end of read-out and earliest begin of a new exposure.
  reg [15:0] PixCount;
  reg [15:0] DelayCount;
  reg [3:0]  State;
  reg [15:0]  ImgRowIndx,ImgcolIndx;
  reg [32:0]  DebugReg;

always @ ( posedge PixClk or negedge Reset)
begin
    if ( Reset == 0 )
	begin
		ParallelVideoOut	 <=0;
		LineValidOut <=0;
		FieldValidOut<=0;
		PixCount	 <=0;
		DelayCount	 <=0;
		State		 <=0;
		DebugReg	 <=0;
    end
	
	else
	begin
	 
		case (State)
			
			0: begin
				ParallelVideoOut	 <=0;
				LineValidOut <=0;
				FieldValidOut<=0;
				PixCount	 <=0;
				DelayCount	 <=0;
				ImgRowIndx  <=0;
				ImgcolIndx  <=0;
				State <= State+1;
			end	
			
			1: begin
				DelayCount<=DelayCount+1;
				if (DelayCount == ExposureTime-1)
				begin
					DelayCount	 <=0;
					State <= State+1;
				end
			  end
			  
			2: begin
				DelayCount<=DelayCount+1;
				if (DelayCount == CPRE-1)
				begin
					DelayCount	 <=0;
					FieldValidOut<=1;
					State <= State+1;
				end
			  end
			  
			3: begin
				DelayCount<=DelayCount+1;
				if (DelayCount == FirsetLinePause-1)
				begin
					DelayCount	 <=0;
					State <= State+1;
				end
			  end
			  
			4: begin
				LineValidOut <=1;
				ImgRowIndx<=ImgRowIndx+1;
				ParallelVideoOut[7:0]<= ImgRowIndx%64 <<2;

				if (ImgRowIndx == FrameWidth)
				begin
					LineValidOut <=0;
					ImgRowIndx<=0;
					if (ImgcolIndx == FrameHeight-1)
					    State <= State+2;//LP Delay
					else
					begin
						ImgcolIndx<=ImgcolIndx+1;
						State <= State+1;//Line Delay
					end	
				end
			  end
			  
			5: begin
				DelayCount<=DelayCount+1;
				if (DelayCount == LinePause-1)
				begin
				    LineValidOut <=/*1*/0;
					DelayCount	 <=0;
					State <= 4;
				end
			  end
			  
			6: begin
				DelayCount<=DelayCount+1;
				if (DelayCount == LastLinePause-1)
				begin
					DelayCount	 <=0;
					FieldValidOut<=0;
					State <= 0;
				end
			  end
		
		endcase 
    end
end	
endmodule	
