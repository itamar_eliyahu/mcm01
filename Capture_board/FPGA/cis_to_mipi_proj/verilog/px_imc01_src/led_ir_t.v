/** 
 ** LED_IR_T Control
 ** Thomas Wang, 2019/09/10
 **/

`include "compiler_directives.v"
`timescale 1 ns / 10 ps

module LED_IR_T
(
	input wire reset_n,
	input wire clock,
	
	input wire led_r,
	input wire led_ir,
	
	output wire led_ir_t
);

	localparam IR_T_IDLE  = 5'b00001;
	localparam LED_R_ON   = 5'b00010;
	localparam LED_R_OFF  = 5'b00100;
	localparam LED_IR_ON  = 5'b01000;
	localparam LED_IR_OFF = 5'b10000;
	
	reg reg_ir;
	reg [4:0] state_ir;
	reg [4:0] state_n;
	
	always @(posedge clock or negedge reset_n)
		if (~reset_n) 
			state_ir <= IR_T_IDLE;
		else
			state_ir <= state_n;

	always @(*)
	begin
		case(state_ir)
		IR_T_IDLE:
		begin
			if (reg_ir == 1'b1)
				state_n <= LED_IR_ON;
			else
				state_n <= IR_T_IDLE;
		end

		LED_IR_ON:
		begin
			if (led_ir == 1'b0)
				state_n <= LED_IR_OFF;
			else
				state_n <= LED_IR_ON;
		end
		
		LED_IR_OFF:
		begin
			if (led_r == 1'b1)
				state_n <= LED_R_ON;
			else
				state_n <= LED_IR_OFF;
		end

		LED_R_ON:
		begin
			if (led_r == 1'b0)
				state_n <= IR_T_IDLE;
			else
				state_n <= LED_R_ON;
		end			
		
		default:
			state_n <= 'bx;

		endcase		
	end
	
	/** FSM output logic **/
	always @(posedge clock)
	begin
		case(state_ir)
		IR_T_IDLE:
		begin
			reg_ir <= 1'b0;
		end
		
		default:
			reg_ir <= 1'b0;
		endcase
	end
	
	assign led_ir_t = reg_ir;

endmodule	// LED_IR_T

