/****************************
 ** LED_test, Breath LED 
 ** Input: clock, rst_n
 ** Output: led[2:0]
 ****************************/
`include "compiler_directives.v"
//`timescale 1 ns / 10 ps

`ifdef ENABLE_BreathLED1
module LED_TEST
#(parameter CLK_Freq = 50000000)
(
	input wire clock,
	output reg [2:0] led
);

	reg[31:0]	clock_cnt;

	// swap per each second @50MHz
	always @(posedge clock)
	begin
		if(clock_cnt < CLK_Freq)
			clock_cnt <= clock_cnt + 1;
		else
			clock_cnt <= 0;

	//	clock_cnt <= (clock_cnt < 50000001)? clock_cnt + 1 | clock_cnt = 0;
	end

	// LED[0] 50MHz
	always @(posedge clock)
	begin
		if(clock_cnt > (CLK_Freq/2))
			led[0] <= ~led[0];
	end  

	//LED[1] 50MHz
	always @(posedge clock)
	begin
		if(clock_cnt > (CLK_Freq/3))
			led[1] <= ~led[1];
	end  

	//LED[2] 50MHz
	always @(posedge clock)
	begin
		if(clock_cnt > (CLK_Freq/4))
			led[2] <= ~led[2];
	end  

endmodule
`endif


/**
 ** Free Counter1
 **/
`ifdef ENABLE_COUNTER1
module COUNTER1
//#(parameter CntSize = 10)
(
	input wire clock,
	output reg	[31:0] Q
);
	always @(posedge clock) 
	begin 
//		if	(Q > 16'hfff0)
//			Q <= 0;
//		else begin
			Q <= Q + 1'b1;
//		end
	end
endmodule	// COUNTER1
`endif
