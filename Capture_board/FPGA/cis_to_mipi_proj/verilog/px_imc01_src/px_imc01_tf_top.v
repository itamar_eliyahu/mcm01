/**
 ** Filename	: top.v
 ** Workfiles	: D:\Workfile_Lattice\px_imc01_tf
 ** Compiler	: Synplify
 ** Description	: Test Pattern for F429. Two CIS and 38 sensors capture card with DVP output 
 ** Update		: Nov/25/2019, Rev 0.0
 ** Target      : LCMXO2-2000HC
 ** Designer	: Thomas Wang
 **/

/**
 **  V72 | FIFO |
 **             |
 **  V72 | FIFO | >> Arbitor >> B-RAM >> DVP_OUTPUT [H/V SYNC]
 **             |
 **  SPI | RAM  |
 **
 **  FSM / ARBITOR
 **/

// ! LCMXO2, NC pin 31,63,103 

//`default_nettype none
//`define FPGA_LCMXO2_2000HC

`include "compiler_directives.v"

module px_imc01_tf_top
(
	/** Roller Encoder **/
	//input		iROLLER,

	/** Clock Input **/
	input wire OSC_50MHz,

	`ifdef  FPGA_LCMXO2_2000HC
		input wire    FCLK_100Mhz,  
		input wire    FCLK_83Mhz,  
		input wire    FCLK_12_5Mhz,
	`endif

	input wire reset_n,			//
	output wire [1:0] oCLK_V72,		// for HT82V72 system clock

    /** HT82V72 #1 **/
	input wire [15:0] CIS1_D,
	input wire CIS1_VS,
	input wire CIS1_HS,
	input wire CIS1_PCLK,
//	input wire CIS1_SP,
//	input wire CIS1_FRM_START,
//	input wire CIS1_RL_IN,
//	input wire CIS1_RL_OUT,

/** LED monitor or control, reserved **/
	input wire oCIS1_RLED,
	input wire oCIS1_GLED,
	input wire oCIS1_BLED,
	input wire oCIS1_IRLED,  // ! LCMXO2, pin 103, NC
	input wire oCIS1_UVLED,


    /** HT82V72 #2 **/ 	
	input wire [15:0] CIS2_D,
	input wire CIS2_VS,
	input wire CIS2_HS,
	input wire CIS2_PCLK,
//	input wire CIS2_SP,
//	input wire CIS2_FRM_START,
//	input wire CIS2_RL_IN,
//	input wire CIS2_RL_OUT,

/** LED monitor or control, reserved **/
	input wire	oCIS2_RLED,
	input wire	oCIS2_GLED,
	input wire	oCIS2_BLED,
	input wire	oCIS2_IRLED,
	input wire	oCIS2_UVLED,

/** Transmitte LED control **/
	output wire	oLED_R_T,
	output wire	oLED_G_T,
	output wire	oLED_B_T,
	output wire	oLED_IR_T,
	

/** M4 Removed, SPI slave only **/
	input wire SPI_SI,
	output wire SPI_SO,
	input wire SPI_SCK,
	input wire SPI_CSSN,

/** DVP **/
//	input wire	iDVP_MCLK,
	output wire	oDVP_PCLK,		// dvp p_clock
	output wire	[7:0] oDVP_D,	// Data bus 8 bit
	output wire	oDVP_VS,		// dvp v-sync
	output wire	oDVP_HS,		// dvp h-sync
//	output wire	oDVP_STBY_EN,	// reserved

    /** MIPI CSI-2 **/
	input wire	LP00,
	input wire	LP01,
	input wire	LP10,
	input wire	LP11,
	input wire	LPCLK0,
	input wire	LPCLK1,
	input wire	D0_p,	
	input wire	D0_n,
	input wire	D1_p,	
	input wire	D1_n,
	input wire	DCK_p,	
	input wire	DCK_n,	

//	output wire	reg oPixel_valid,
//	output wire	reg oDVP_frame_done,

	output wire	TP19,		//
//	output wire	TP31,		//
	output wire	TP42,		//
//	output wire	TP46,		// 

//	input wire	I2C_SCL_FPGA,	// connected to MCU
//	inout wire	I2C_SDA_FPGA,	// connected to MCU

//	input wire	[3:0] CIS_MODE,		// connect to MCU

// LEDs
	output wire [2:0] oLED		// LEDs for status
);
/* synthesis syn_force_pads=1 syn_noprune = 1 */


/**
	CIS port pass through to DVP as debug
 **/
`ifdef ENABLE_DVP_F429
`ifdef LINK_V72_P1_DVP 
//	assign #1 oDVP_D[7:0] = CIS1_D[7:0];
	assign #1 oDVP_D[7:0] = CIS1_D[15:8];
	assign #1 oDVP_VS = CIS1_VS;
	assign #1 oDVP_HS = CIS1_HS;
	assign #1 oDVP_PCLK = CIS1_PCLK;
	assign #1 oDVP_STBY_EN = CIS1_PCLK;
`endif
//`endif

	//`ifdef ENABLE_DVP_F429
`ifdef LINK_V72_P2_DVP 
//	assign #1 oDVP_D[7:0] = CIS2_D[7:0];
	assign #1 oDVP_D[7:0] = CIS2_D[15:8];
	assign #1 oDVP_VS = CIS2_VS;
	assign #1 oDVP_HS = CIS2_HS;
	assign #1 oDVP_PCLK = CIS2_PCLK;
	assign #1 oDVP_STBY_EN = CIS2_PCLK;
`endif
`endif

//`ifdef DEBUG_EBR
//`ifdef DEBUG_CAP
//`ifdef DEBUG_DVP
`ifdef DEBUG_MONITOR
	wire [`debug_pin-1:0] debug_o;
//	assign TP46  	= debug_o[11];	// 
//	assign TP42  	= debug_o[10];	// 
//	assign TP31  	= debug_o[10];	// 
//	assign TP19  	= debug_o[9];	// 
	assign DCK_p  	= debug_o[8];	// R115
	assign DCK_n  	= debug_o[7];	// R111
	assign D1_p  	= debug_o[6];	// R109
	assign D1_n  	= debug_o[5];	// R106
	assign D0_p 	= debug_o[4];	// R105
	assign D0_n 	= debug_o[3];	// R102
	assign oLED[2]  = debug_o[2];	// LED4
	assign oLED[1]  = debug_o[1];	// LED5
	assign oLED[0]  = debug_o[0];	// LED6
`endif


/**
 ** System parameters 
 **/
//	parameter	CLK_Freq = 50000000;
//	parameter	SysCountLen = 32;
//	wire [SysCountLen-1:0] SysCountData = 32'h0000_0000;

	`define SysCountLen 32
	wire [`SysCountLen-1:0] SysCountData = 32'h0000_0000;

/**
 ** HT82V72 parameters
 **/
	parameter CIS_size   	= 1440; //1440
	parameter LED_colors 	= 8;
	parameter M4_sensors 	= 38; // 38
	parameter DVP_pixels 	= 2918; //2918
	parameter CIS_CHIP_PIXEL = 288; 
//	parameter Index_Bit 	= 12;

//	GSR u_GSR(1'b1);
//	GSR GSR_INST(.GSR(reset_n));


//`ifdef OSC_INT_CLOCK
`ifdef ENABLE_OSCH

/**
 ** Ref: MachXO2sysCLOCKPLLDesignandUsageGuide.pdf
 ** Default: 2.08
 ** 2.08, 2.15, 2.22, 3.25, 38.00, 44.33, 53.20, 66.5, 88.67, 133.0
 **/
	wire CLK_OSCH;
	defparam OSCH_inst.NOM_FREQ = "133.0"; // 
	OSCH OSCH_inst( 
		.STDBY(1'b0), 		// 0=Enabled, 1=Disabled also Disabled with Bandgap=OFF
		.OSC(CLK_OSCH), 
		.SEDSTDBY()     	// this signal is not required if not using SED
		) /* synthesis syn_noprune=1 syn_keep=1 */;

//	assign #1 OSC_TP = ~OSC_50MHz;
`endif


/**
 ** PLL
 ** Input: 50MHz
 ** Output: 120MHz / 12MHz;
 **       : 120/12/80 2019-12-23
 **/
   
    `ifdef  FPGA_LCMXO2_2000HC
		wire	CLK_CAP = FCLK_100Mhz;
		wire	CLK_V72 = FCLK_12_5Mhz;	// clock source, 12MHz
		wire 	CLK_DVP = FCLK_100Mhz;//FCLK_83Mhz;	
    
	`else

		wire	CLK_CAP;
		wire	CLK_V72;	// clock source, 12MHz
		wire 	CLK_DVP;	
		PLL_120_12 u_PLL_120_12
		(
			.CLKOP(CLK_CAP),	// output 100MHz
			.CLKOS(CLK_V72),	// output 12MHz
			.CLKOS2(CLK_DVP),	// output 80MHz
			.CLKI(OSC_50MHz) 	// input OSC_50MHz
		);

    `endif
	/** must keep **/
	assign oCLK_V72[0] = CLK_V72;
	assign oCLK_V72[1] = CLK_V72;


/**
 ** BRAM Interface
 **/
	/** HT82V72_M4 output to BRAM **/
	wire cap_pclk;
	wire [7:0] cap_data;
	wire [11:0] cap_index;
	wire cap_write;
	wire cap_busy;

	/** BRAM output to DVP **/
	reg  dvp_vs_i;
	reg  dvp_hs_i;
	wire bram_vs_o;
	wire bram_hs_o;
	wire bram_pclk;
	wire [7:0] bram_data;
	wire [11:0] bram_index;	 // 12bit for 2880

	//assign dvp_vs_i = CIS1_VS;
	//assign dvp_hs_i = CIS1_HS;
	//assign dvp_vs_i = CIS1_VS | CIS2_VS;
	//assign dvp_hs_i = CIS1_HS | CIS2_HS;

	always @(*)
		dvp_vs_i = CIS1_VS | CIS2_VS;
	
	always @(*)
		dvp_hs_i = CIS1_HS | CIS2_HS;

//	assign oDVP_D[7:0] = (oDVP_HS == 1'b1)? bram_data[7:0]: 8'b0;
//	assign oDVP_D[7:0] = bram_data[7:0];

/**
 ** HT82V72 & M4
 ** Parameter:
 **/
	HT82V72_M4 
	#(
		.CIS_size(CIS_size),
		.LED_colors(LED_colors),
		.M4_sensors(M4_sensors)
	) 
	u_HT82V72_M4
	(
		.reset_n(reset_n),
		.clock(CLK_CAP), // Max: 100MHz@MX2-2000HC

		// V72, U1
		.vsync1(CIS1_VS),
		.hsync1(CIS1_HS),
		.pclk1(CIS1_PCLK),
		.data_i1(CIS1_D),

		// V72, U2
		.vsync2(CIS2_VS),
		.hsync2(CIS2_HS),
		.pclk2(CIS2_PCLK),
		.data_i2(CIS2_D),

`ifdef DEBUG_CAP
		/** debug **/
		.debug_o(debug_o), //
`endif
		// to BRAM
		.data_o(cap_data),		// cap_data
		.pclk_o(cap_pclk),		// cap_pclk
		.index_o(cap_index), //(cap_index)	// 14bit, 1440 * 6 = 8640
		.write_en(cap_write)
	//	.cap_busy(cap_busy)
	);


/** Tx **/
`ifdef DEBUG_CAP1
	assign oDVP_D[7:0] = cap_index[7:0];
	assign oDVP_VS = bram_vs_o;
	assign oDVP_HS = bram_hs_o;
	assign oDVP_PCLK = cap_pclk;
	assign oDVP_STBY_EN = cap_pclk;
`endif


/** EBR example
(
	.WrAddress( addr_rd[3:0]),
	.Data     ( din_rd),
	.WrClock  ( clk),
	.WE       ( wren_rd),
	.WrClockEn( hi_val),
	.RdAddress( addr_rb[3:0]),
	.RdClock  ( clk),
	.RdClockEn( hi_val),
	.Reset    ( lo_val),
	.Q        ( dout_rb)
);
 **/
	LT_BRAM u_LT_BRAM
	(
		.Reset(!reset_n), //input reseta

		.WrClockEn(1'b1), //input wrea
		.WrClock(cap_pclk), // OSC_50MHz, CLK_125MHz, //input clkb
		.WE(cap_write), //input wreb, 'TRUE, 1'b1
		.WrAddress(cap_index), //input [11:0] ada
		.Data(cap_data), //input [7:0] din	

		.RdClockEn(1'b1), 
		.RdClock(bram_pclk), //OSC_50MHz, cap_pclk, //input clka
		.RdAddress(bram_index), //input [11:0] adb
		.Q(bram_data) //output [7:0] dout
	);

`ifdef DEBUG_EBR
//	assign #1 debug_o[11] = bram_data[3]; 	// TP46
//	assign #1 debug_o[10] = bram_data[2]; 	// TP42 
//	assign #1 debug_o[9]  = bram_data[1];	// TP19
	assign #1 debug_o[8]  = bram_data[0];	// R115 (Left)

	assign #1 debug_o[7]  = bram_index[1];	// R111
	assign #1 debug_o[6]  = bram_index[0];	// R109
	assign #1 debug_o[5]  = cap_data[1];	// R106
	assign #1 debug_o[4]  = cap_data[0];	// R105

	assign #1 debug_o[3]  = cap_index[1];	// R102 (right)
	assign #1 debug_o[2]  = cap_index[0];	// LED6
	assign #1 debug_o[1]  = cap_write;	// LED5
	assign #1 debug_o[0]  = cap_pclk;	// LED4
`endif


/** 
 ** DVP section
 **/
	//parameter	total_cols = 2918;
	//parameter	total_rows = 8;
	//parameter	h_pixels = 2918;   // pixels_cis1 + pixels_cis2 + M3_sensors
	//parameter	v_lines = 8;

/** 
 ** DVP V/H Sync pulse generator
 **/
	DVP_OUTPUT 
	#(
		.DVP_pixels(DVP_pixels),
		.LED_colors(LED_colors),
		.M4_sensors(M4_sensors)
	)
	u_DVP_OUTPUT
	(
		.reset_n(reset_n),
		.clock(CLK_DVP),		// [OSC] 250/8 = 31.25MHz
	//	.clock_sys(OSC_50MHz),

	/** debug **/
`ifdef	DEBUG_DVP
	.debug_o(debug_o),	
`endif
	// input port
	.vsync_i(dvp_vs_i),
	.hsync_i(dvp_hs_i),

	// output port	
	.index_o(bram_index),	// output reg [11:0] 
	.vsync_o(bram_vs_o),
	.hsync_o(bram_hs_o),
	.pclk_o(bram_pclk) //(oDVP_PCLK)
);

`ifdef ENABLE_DVP_SYNC
//	assign oDVP_D[7:0] = (oDVP_HS == 1'b1)? bram_data[7:0]: 8'b0;
	assign oDVP_D[7:0] = bram_data[7:0];
	assign oDVP_PCLK = bram_pclk;
	assign oDVP_VS = bram_vs_o;
	assign oDVP_HS = bram_hs_o;	
	assign oLED[1] = CIS2_VS;	// LED5
	assign oLED[0] = CIS1_VS;	// LED4
//	assign oDVP_STBY_EN = bram_pclk;

`endif	// ENABLE_DVP_SYNC 

`ifdef DEBUG_DVP1
//	assign #1 debug_o[11] = bram_data[3]; 	// TP46
//	assign #1 debug_o[10] = bram_data[2]; 	// TP31 
//	assign #1 debug_o[9]  = bram_data[1];	// TP19
	assign #1 debug_o[8]  = bram_pclk;		// R115 (Left)

	assign #1 debug_o[7]  = bram_index[7];	// R111
	assign #1 debug_o[6]  = bram_index[6];	// R109
	assign #1 debug_o[5]  = bram_index[5];	// R106
	assign #1 debug_o[4]  = bram_index[4];	// R105

	assign #1 debug_o[3]  = bram_index[3];	// R102 (right)
	assign #1 debug_o[2]  = bram_index[2];	// LED6
	assign #1 debug_o[1]  = bram_index[1];	// LED5
	assign #1 debug_o[0]  = bram_index[0];	// LED4
`endif


/** 
 ** SPI slave
 **/
`ifdef ENABLE_SPI_SLAVE

	wire M4_CLK;   // Memory clock (copy of CLK)
	wire M4_WR;    // Memory write enable                     
	wire [7:0] M4_ADDR;  // Memory address
	wire [7:0] M4_WD;    // Memory write data
	wire [7:0] M4_RD;    // Memory read data

	spi_slave_top
	#(
		.GPI_PORT_NUM(),      // GPI port number, [4]
		.GPI_DATA_WIDTH(),    // GPI data width
		.GPO_PORT_NUM(),      // GPO port number, [4]
		.GPO_DATA_WIDTH(),    // GPO data width  
		.MEM_ADDR_WIDTH(),    // Memory addrss width
		.IRQ_NUM(),           // Interrupt request number, [4]
		.REVISION_ID(),   // Revision ID
		.MAX_MEM_BURST_NUM(38), // Maximum memory burst number, [8]
		.INTQ_OPENDRAIN()  // INTQ opendrain setting (ON/OFF), [ON]
	)
	u_spi_slave
	(
		.CLK(OSC_50MHz),       // System clock
		.RST_N(reset_n),     // System asynchronouse reset (active low)
												
		.CCLK(SPI_SCK),      // Hard SPI serial clock
		.SCSN(SPI_CSSN),      // Hard SPI chip-select (active low)
		.SI(SPI_SI),        // Hard SPI serial input data
		.SO(SPI_SO),        // Hard SPI serial output data
	
	// GPI/GPO ports can be defined manually based on the real case. And the parameters                                             
	// such as GPI_PORT_NUM/GPI_DATA_WIDTH/GPO_PORT_NUM/GPO_DATA_WIDTH should be set accordingly.
		.GPI_0(),     // General purpose input port 0  
		.GPI_1(),     // General purpose input port 1 
		.GPI_2(),     // General purpose input port 2 
		.GPI_3(),     // General purpose input port 3 
		.GPO_0(),     // General purpose output port 0 
		.GPO_1(),     // General purpose output port 1 
		.GPO_2(),     // General purpose output port 2 
		.GPO_3(),     // General purpose output port 3 
		.Enable(),    // Genaral purpose enable
	
		.MEM_CLK(M4_CLK),   // Memory clock (copy of CLK)
		.MEM_WR(M4_WR),    // Memory write enable                     
		.MEM_ADDR(M4_ADDR),  // Memory address
		.MEM_WD(M4_WD),    // Memory write data
		.MEM_RD(M4_RD),    // Memory read data
	
		.IRQ(),       // Interrupt requests (rising edge sensitive)       
		.INTQ()       // External interrupt (active low)    
	);

`endif


/**
 ** I2C interface
 **/
`ifdef ENABLE_I2C_SLAVE
	assign #1 I2C_SCL_FPGA = 1'bz;
	assign #1 I2C_SDA_FPGA = 1'bz;
`endif


/**
 ** LED Transmitte Control
 ** 2019-12-10
 **/
	assign oLED_IR_T = (!oCIS1_IRLED & oCIS2_IRLED)? 1'b1: 1'b0;

	assign oLED_R_T = (!oCIS1_RLED & oCIS2_RLED)? 1'b1: 1'b0;
	assign oLED_G_T = (!oCIS1_GLED & oCIS2_GLED)? 1'b1: 1'b0;
	assign oLED_B_T = (!oCIS1_BLED & oCIS2_BLED)? 1'b1: 1'b0;

endmodule		// end of TOP