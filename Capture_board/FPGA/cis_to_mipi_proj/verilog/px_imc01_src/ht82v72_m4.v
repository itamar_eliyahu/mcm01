/** 
 ** HT82V72 & M4
 ** VPFE I/F : HT82V72
 ** SPI I/F : M4
 ** FIFO 16b-to-8bit x2
 ** FSM functions
 ** This module provides the state machine 
 ** for HT82V72 2chips and SPI collect
 ** HT82V72 copy datas to FIFO when HSYNC assert
 ** Move FIFO datas to EBR when HSYNC de-assert
 **
 ** -- parameterized module instance --
 ** Ref: Memory Usage Guide for MachXO2 Devices.pdf 
 ** Thomas Wang, 2019-12-04
 **
 **/

`include "compiler_directives.v"

module HT82V72_M4
#(
	parameter CIS_size = 1440,
	parameter LED_colors = 8,
	parameter M4_sensors = 38	
)
(
	input wire reset_n,
	input wire clock,	// clock for Capture

`ifdef DEBUG_CAP
/** debug port **/
	output wire [`debug_pin-1:0] debug_o,
`endif

/** HT82V72 #1 **/
	input wire vsync1,
	input wire hsync1,
	input wire pclk1,
	input wire [15:0] data_i1,	// HT82V72 16bit output

/** HT82V72 #2 **/
	input wire vsync2,
	input wire hsync2,
	input wire pclk2,
	input wire [15:0] data_i2,	// HT82V72 16bit output

/** output port **/
	output wire [11:0] index_o,	// 12bit
	output wire [7:0] data_o,	// cap_data
	output wire pclk_o,			// cap_pclk
	
	output wire write_en
//	frame_done
)/* synthesis syn_force_pads=1 syn_noprune = 1 */;


/**
 **	System Registors
 **/
	reg [11:0] index_u1 = 12'h000; // addr, 12bit
	reg [11:0] index_u2 = 12'h000;
	reg fifo_rd_u1 = 1'b0;
	reg fifo_rd_u2 = 1'b0;
	reg fifo_reset  = 1'b0;
	reg dtx_en      = 1'b0;


/**
 **	FIFO for U1
 **/
	wire sync_wr1 = vsync1 & hsync1;
	wire fifo_clk1 = hsync1 & !pclk1; 
	wire [7:0] fifo_o1;
	reg [15:0] fifo_i1; // data, 16bit 
	wire fifo_rst_u1;
	reg [15:0] fifo_cnt16_u1;

	wire fifo_emp1;
	wire fifo_aemp1;
	wire fifo_ful1;
	wire fifo_aful1;

/** NG **/
//	assign #1	fifo_i1[15:8] = (sync_wr1 == 1'b1)? data_i1[7:0]:16'h0000;
//	assign #1	fifo_i1[7:0] = (sync_wr1 == 1'b1)? data_i1[15:8]:16'h0000;

	always @(negedge pclk1)
	begin
		fifo_i1[15:8] <= data_i1[7:0];
		fifo_i1[7:0]  <= data_i1[15:8];
	end

	wire [3:0] clkdiv_pclk1;
	clock_div16 u_clkdiv_hsync1
	(
		.clock(pclk1), 
		.reset(!reset_n), 
		.clock_div16(clkdiv_pclk1)
	);

	edge_detector u_posedge_pclk1
	(
		.clock(pclk1), // (!pclk1)/clkdiv_pclk1[0]) 
		.reset(!reset_n), 
		.level(hsync1), 
		.Mealy_tick(), 
		.Moore_tick(fifo_rst_u1)
	);

	LT_FIFO u_FIFO_U1
	(
		.Data(fifo_i1), // (fifo_i1)/(data_i1),
		.WrClock(fifo_clk1), // (fifo_clk1)/(pclk1)
		.RdClock(clock),
		.WrEn(sync_wr1), // (sync_wr1)/(hsync1)
		.RdEn(fifo_rd_u1),
		.Reset(fifo_reset), // (fifo_reset)(!reset_n),
		.RPReset(fifo_reset), //(fifo_rst_u1)/(fifo_reset),		
		.Q(fifo_o1),
		.Empty(fifo_emp1), //(fifo_emp1),
		.Full(fifo_ful1), //(fifo_ful1),
		.AlmostEmpty(fifo_aemp1), //(fifo_aemp1), 
		.AlmostFull(fifo_aful1) //(fifo_aful1)
	);

/*
	always @(posedge fifo_clk1)
	begin
		if (cnt16_fifo_u1[15:0] < 16'h0003)
			init_fifo_u1 <= 1'b1;
		else
			init_fifo_u1 <= 1'b0;
	end
*/

/**
 **	FIFO for U2
 **/
	wire sync_wr2 = vsync2 & hsync2;
	wire fifo_clk2 = hsync2 & !pclk2;
	wire [7:0] fifo_o2;
	reg [15:0] fifo_i2;	// data, 16 bit
	wire fifo_rst_u2;
	reg [15:0] fifo_cnt16_u2;		

	wire fifo_emp2;
	wire fifo_aemp2;
	wire fifo_ful2;
	wire fifo_aful2;

/** NG **/
//	assign #1	fifo_i2[15:8] = (sync_wr2 == 1'b1)?data_i2[7:0]:16'h0000;
//	assign #1	fifo_i2[7:0] = (sync_wr2 == 1'b1)?data_i2[15:8]:16'h0000;

	always @(negedge pclk2)
	begin
		fifo_i2[15:8] <= data_i2[7:0];
		fifo_i2[7:0]  <= data_i2[15:8];
	end

	wire [3:0] clkdiv_pclk2;
	clock_div16 u_clkdiv_pclk2
	(
		.clock(pclk2), 
		.reset(!reset_n), 
		.clock_div16(clkdiv_pclk2)
	);

	edge_detector u_posedge_hsync2
	(
		.clock(clkdiv_pclk2[0]), // (pclk2), 
		.reset(!reset_n), 
		.level(hsync2), 
		.Mealy_tick(), 
		.Moore_tick(fifo_rst_u2)
	);

	LT_FIFO u_FIFO_U2
	(
		.Data(fifo_i2),
		.WrClock(fifo_clk2), // fifo_clk2, pclk2
		.RdClock(clock),
		.WrEn(sync_wr2),
		.RdEn(fifo_rd_u2),
		.Reset(fifo_reset),// (!reset_n)(fifo_reset),
		.RPReset(fifo_reset), // (fifo_rst_u2),
		.Q(fifo_o2),
		.Empty(fifo_emp2), //(fifo_emp2),
		.Full(fifo_ful2), //(fifo_ful2),
		.AlmostEmpty(fifo_aemp2), //(fifo_aemp2), 
		.AlmostFull(fifo_aful2) //(fifo_aful2)
	);

/*	
	always @(posedge fifo_clk2)
	begin
		if (cnt16_fifo_u2[15:0] < 16'h0003)
			init_fifo_u2 <= 1'b1;
		else
			init_fifo_u2 <= 1'b0;
	end
*/

/**
 **	Buffer for M4
 **/
//	parameter M4_sensors = 38;
	reg [7:0] index_m4 = 8'h00;	// reg for 38byte
	reg [7:0] ram_m4 [0:40] /* synthesis syn_ramstyle="registers" */; // "block_ram"
	reg m4_rd = 1'b0;

	wire [11:0] index_f2b;
	reg [15:0] index_cnt = 16'h0000;
//	reg index_f2b_rst;

/**
 ** posedge/negedge detector for HSYNC
 **/
	wire [3:0] clock_div_cap;
	wire posedge_hs;
	wire negedge_hs;

/** 
 ** Clock div n for edge capture
 **/
	clock_div16 u_clock_div16_cap
	(
		.clock(clock), 
		.reset(!reset_n), 
		.clock_div16(clock_div_cap)
	);

/** 
 ** Edge detect to hsync high edge
 **/
	posedge_det u_posedge_hs
	(
		.reset(!reset_n),
		.sig(hsync1|hsync2),
		.clock(clock), // (clock_div_cap[0]),
		.pe(posedge_hs)
	); 

/** 
 **  Edge detect to hsync low edge
 **/
	posedge_det u_negedge_hs
	(
		.reset(!reset_n),
		.sig(!(hsync1|hsync2)),
		.clock(clock), // (clock_div_cap[0]),
		.pe(negedge_hs)
	); 


/**
 ** FSM for Capture
 ** Ref: Designing Safe Verilog State Machines with Synplify.pdf
 **/
//	localparam CAP_VIDLE  = 8'b0000_0001;
	localparam CAP_DTX    = 8'b0000_0001;
	localparam CAP_HIDLE  = 8'b0000_0010;
	localparam CAP_FIFO   = 8'b0000_0100;
//	localparam CAP_INIT   = 8'b0000_1000;
//	localparam CAP_TX_U1  = 8'b0001_0000;
//	localparam CAP_TX_U2  = 8'b0010_0000;
//	localparam CAP_TX_M4  = 8'b0100_0000;
//	localparam CAP_RESET  = 8'b1000_0000;

	reg [7:0] cap_state;
//	reg [7:0] state_next;

	always @(posedge clock)	// or negedge reset_n)
	begin:FSM
		if(!reset_n)
			cap_state <= #1 CAP_HIDLE;
		else
		case(cap_state)
	/*
		CAP_VIDLE:
		if ((vsync1 | vsync2) == 1'b1)
			cap_state <= #1 CAP_HIDLE;
		else
			cap_state <= #1 CAP_VIDLE;
	*/
		CAP_HIDLE:
		begin
			if (posedge_hs == 1'b1)
				cap_state <= #1 CAP_FIFO;
			else
				cap_state <= #1 CAP_HIDLE;
		end

		CAP_FIFO:
		begin
			if (negedge_hs == 1'b1)
//				cap_state <= #1 CAP_INIT;
				cap_state <= #1 CAP_DTX;
			else
				cap_state <= #1 CAP_FIFO;
		end

		CAP_DTX:
		begin
			if (index_f2b >= 16'd2940)		
				cap_state <= #1 CAP_HIDLE;
			else
				cap_state <= #1 CAP_DTX;
		end 

		default:
			cap_state <= #1 CAP_HIDLE;

		endcase
	end


/** index_cnt **/
	always @(posedge clock or negedge reset_n)
	begin
		if (!reset_n|(cap_state[7:0] == CAP_FIFO)|(index_cnt > 16'd3080))  // ?????????
			index_cnt  <= #1 16'h0000;
		else 
			index_cnt  <= #1 index_cnt + 16'h0001;		
	end

/** 
 **index_f2b
 **/
	wire cnt16_reset;
	wire [15:0] cnt16_f2b;

	assign cnt16_reset = (cap_state[7:0] == CAP_FIFO);
	assign index_f2b[11:0] = cnt16_f2b[11:0];

	LT_COUNTER_16 u_LT_COUNTER_16
	(
		.Clock(clock),
		.Clk_En(1'b1), 
		.Aclr(cnt16_reset), 
		.Q(cnt16_f2b)
	);

	always @(posedge clock)
	begin
		if (cap_state[7:0] == CAP_DTX)
		begin
			if (index_f2b < 12'd1440)	// tx, U1
			begin
				dtx_en     <= #1 1'b1;
				fifo_rd_u1 <= #1 1'b1;
				fifo_rd_u2 <= #1 1'b0;
				m4_rd      <= #1 1'b0;
				fifo_reset <= #1 1'b0;
			end
			else
			if (index_f2b < 12'd2880)	// tx, U2
			begin
				dtx_en     <= #1 1'b1;
				fifo_rd_u1 <= #1 1'b0;
				fifo_rd_u2 <= #1 1'b1;
				m4_rd      <= #1 1'b0;
				fifo_reset <= #1 1'b0;
			end
			else
			if (index_f2b < 12'd2918)	 // tx, M4 
			begin
				dtx_en     <= #1 1'b1;
				fifo_rd_u1 <= #1 1'b0;
				fifo_rd_u2 <= #1 1'b0;
				m4_rd      <= #1 1'b1;
				fifo_reset <= #1 1'b0;
			end
			else
			if (index_f2b < 12'd2928)	// fifo reset assert
			begin
				dtx_en     <= #1 1'b0;
				fifo_rd_u1 <= #1 1'b0;
				fifo_rd_u2 <= #1 1'b0;
				m4_rd      <= #1 1'b0;
				fifo_reset <= #1 1'b1;
			end
			else
			if (index_f2b < 12'd2938)	// fifo reset de-assert
			begin
				dtx_en     <= #1 1'b0;
				fifo_rd_u1 <= #1 1'b0;
				fifo_rd_u2 <= #1 1'b0;
				m4_rd      <= #1 1'b0;
				fifo_reset <= #1 1'b0;
			end
		end
	end

/*	
	assign #1 fifo_rd_u1 = (cap_state[7:0] == CAP_TX_U1)? 1'b1: 1'b0;
	assign #1 fifo_rd_u2 = (cap_state[7:0] == CAP_TX_U2)? 1'b1: 1'b0;
	assign #1 m4_rd      = (cap_state[7:0] == CAP_TX_M4)? 1'b1: 1'b0;
	assign #1 fifo_reset = (cap_state[7:0] == CAP_RESET)? 1'b1: 1'b0;
*/

//	assign #1 write_en   = (fifo_rd_u1 | fifo_rd_u2 | m4_rd)? 1'b1: 1'b0;
	assign #1 write_en   = (dtx_en == 1'b1)? 1'b1: 1'b0;
//	assign #1 write_en   = dtx_en;
	assign #1 index_o[11:0] = (dtx_en == 1'b1)? index_f2b[11:0]: 12'h000;
	assign #1 pclk_o = clock; // (dtx_en == 1'b1)? clock: 1'b0;
	assign #1 data_o[7:0] = (fifo_rd_u1)? fifo_o1[7:0]:
							(fifo_rd_u2)? fifo_o2[7:0]:
							(m4_rd)? ram_m4[index_m4]: 8'h00;


`ifdef DEBUG_CAP
//	assign #1 debug_o[12] = index_f2b[2]; 	// TP46
//	assign #1 debug_o[11] = index_f2b[2]; 	// TP42
//	assign #1 debug_o[10] = index_f2b[1]; 	// TP31 
//	assign #1 debug_o[9]  = index_f2b[0];	// TP19
	assign #1 debug_o[8]  = data_o[2];		// R115 (Left)

	assign #1 debug_o[7]  = fifo_o2[0];	// R111
	assign #1 debug_o[6]  = fifo_o1[0];	// R109
	assign #1 debug_o[5]  = fifo_clk2;	// R106
	assign #1 debug_o[4]  = fifo_i2[0];	// R105

	assign #1 debug_o[3]  = fifo_clk1;	// R102 (right)
	assign #1 debug_o[2]  = fifo_i1[0];	// LED6
	assign #1 debug_o[1]  = negedge_hs;	// LED5
	assign #1 debug_o[0]  = posedge_hs;	// LED4
`endif

endmodule	// HT82V72_M4
