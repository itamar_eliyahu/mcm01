



`timescale  1ns / 1ps
`include "compiler_directives.v"




module px_imc01_tf_with_tx_mipi_bridge_top
   #(
    // px_imc01_tf_top Parameters
    //parameter PERIOD          = 10     ,
    parameter CIS_size        = 1440   ,
    parameter LED_colors      = 8      ,
    parameter M4_sensors      = 38     ,
    parameter DVP_pixels      = 2918   ,
    parameter CIS_CHIP_PIXEL  = 288    ,
	// mipi_tx_bridge_top Parameters


     parameter              VC         = 0             ,  //2-bit Virtual Channel Number
     parameter              WC         = 16'h0258      ,  //16-bit Word Count in byte packets.  16'h05A0 = 16'd1440 bytes = 1440 * (8-bits per byte) / (24-bits per pixel for RGB888) = 480 pixels
     parameter              word_width = 8            ,  //Pixel Bus Width.  Example: RGB888 = 8-bits Red, 8-bits Green, 8-bits Blue = 24 bits/pixel
     parameter              DT         = 6'h2A         ,  //6-bit MIPI CSI2 Data Type.  Example: dt = 6'h2B = RAW10
     parameter              testmode   = 0             ,  //adds colorbar pattern generator for testing purposes.  Operates off of PIXCLK input clock and reset_n input reset
     parameter              crc16      = 1             ,  //appends 16-bit checksum to the end of long packet transfers.  0 = off, 1 = on.  Turning off will append 16'hFFFF to end of long packet.  Turning off will reduce resource utilization.
     parameter              reserved   = 0             ,   //reserved=0 at all times
     parameter              lane_width = 2  
)

(
    // px_imc01_tf_top
	/** Clock Input **/
	input wire OSC_50MHz,
	//input wire reset_n,			//
	output wire [1:0] oCLK_V72,		// for HT82V72 system clock

    /** HT82V72 #1 **/
	input wire [15:0] CIS1_D,
	input wire CIS1_VS,
	input wire CIS1_HS,
	input wire CIS1_PCLK,

     /* for test only*/
     input LineValid,
     input FieldValid,
     input [7:0]ParallelVideo,


/** LED monitor or control, reserved **/
	input wire oCIS1_RLED,
	input wire oCIS1_GLED,
	input wire oCIS1_BLED,
	input wire oCIS1_IRLED,  // ! LCMXO2, pin 103, NC
	input wire oCIS1_UVLED,


    /** HT82V72 #2 **/ 	
	input wire [15:0] CIS2_D,
	input wire CIS2_VS,
	input wire CIS2_HS,
	input wire CIS2_PCLK,

/** LED monitor or control, reserved **/
	input wire	oCIS2_RLED,
	input wire	oCIS2_GLED,
	input wire	oCIS2_BLED,
	input wire	oCIS2_IRLED,
	input wire	oCIS2_UVLED,

/** Transmitte LED control **/
	output wire	oLED_R_T,
	output wire	oLED_G_T,
	output wire	oLED_B_T,
	output wire	oLED_IR_T,
	

/** M4 Removed, SPI slave only **/
	input wire SPI_SI,
	output wire SPI_SO,
	input wire SPI_SCK,
	input wire SPI_CSSN,

/** DVP **/
  /*
	output wire	oDVP_PCLK,		// dvp p_clock
	output wire	[7:0] oDVP_D,	// Data bus 8 bit
	output wire	oDVP_VS,		// dvp v-sync
	output wire	oDVP_HS,		// dvp h-sync
*/
    /** MIPI CSI-2 **/
	// input wire	LP00,
	// input wire	LP01,
	// input wire	LP10,
	// input wire	LP11,
	// input wire	LPCLK0,
	// input wire	LPCLK1,
	input wire	D0_p,	
	input wire	D0_n,
	input wire	D1_p,	
	input wire	D1_n,
	input wire	DCK_p,	
	input wire	DCK_n,	

	output wire	TP19,		//
	output wire	TP42,		//

	output wire [2:0] oLED	,	// LEDs for status
	
	
	//tx_mipi_bridge
	 input            reset_n,  // resets design (active low)
                                                                          
     output            DCK,  // HS (High Speed) Clock                            
                                                                         
     //`ifdef HS_3                                                                      
         // output        D3,  //HS (High Speed) Data Lane 3         
          //output        D2,  //HS (High Speed) Data Lane 2         
          //output        D1,  //HS (High Speed) Data Lane 1         
          //output        D0//HS (High Speed) Data Lane 0         
 
     //`elsif HS_2 
          //output        D2,      
          //output        D1,
          //output        D0
                                                       
     //`elsif HS_1                                       
          output        D1,
          output        D0,
                                                       
     //`elsif HS_0                                       
         // output        D0                                             
     //`endif  
     
                                               
  /**/  // `ifdef LP_CLK                                     
          inout   [1:0] LPCLK                          ,  //LP (Low Power) External Interface Signals for Clock Lane     
     //`endif                                                                                                              
     //`ifdef LP_3                                                                                                         
     //     inout   [1:0] LP3                            ,  //LP (Low Power) External Interface Signals for Data Lane 3    
     //`endif                                                                                                              
     //`ifdef LP_2                                                                                                         
     //     inout   [1:0] LP2                            ,  //LP (Low Power) External Interface Signals for Data Lane 2    
     //`endif                                                                                                              
     //`ifdef LP_1                                                                                                         
          inout   [1:0] LP1                              //LP (Low Power) External Interface Signals for Data Lane 1    
     //`endif                                                                                                              
     //`ifdef LP_0                                                                                                         
     //     inout   [1:0] LP0                              //LP (Low Power) External Interface Signals for Data Lane 0    
     //`endif                                                                                                               
                        


	

	
   );



`ifdef CIS_TO_DVP

	wire	oDVP_PCLK,FCLK_12_5Mhz,FCLK_83_Mhz,FCLK_100Mhz;		// dvp p_clock
	wire	[7:0] oDVP_D;	// Data bus 8 bit
	wire	oDVP_VS;		// dvp v-sync
	wire	oDVP_HS;		// dvp h-sync
	
px_imc01_tf_top #(
    .CIS_size       ( CIS_size       ),
    .LED_colors     ( LED_colors     ),
    .M4_sensors     ( M4_sensors     ),
    .DVP_pixels     ( DVP_pixels     ),
    .CIS_CHIP_PIXEL ( CIS_CHIP_PIXEL ))
 u_px_imc01_tf_top (
    .OSC_50MHz               ( OSC_50MHz           ),
    `ifdef  FPGA_LCMXO2_2000HC
        .FCLK_12_5Mhz            ( FCLK_12_5Mhz        ),
        .FCLK_100Mhz             ( FCLK_100Mhz         ),
        .FCLK_83Mhz              ( FCLK_83Mhz          ),
    `endif
    .reset_n                 ( reset_n             ),
    .CIS1_D                  ( CIS1_D       [15:0] ),
    .CIS1_VS                 ( CIS1_VS             ),
    .CIS1_HS                 ( CIS1_HS             ),
    .CIS1_PCLK               ( CIS1_PCLK           ),
    .oCIS1_RLED              ( oCIS1_RLED          ),
    .oCIS1_GLED              ( oCIS1_GLED          ),
    .oCIS1_BLED              ( oCIS1_BLED          ),
    .oCIS1_IRLED             ( oCIS1_IRLED         ),
    .oCIS1_UVLED             ( oCIS1_UVLED         ),
    .CIS2_D                  ( CIS2_D       [15:0] ),
    .CIS2_VS                 ( CIS2_VS             ),
    .CIS2_HS                 ( CIS2_HS             ),
    .CIS2_PCLK               ( CIS2_PCLK           ),
    .oCIS2_RLED              ( oCIS2_RLED          ),
    .oCIS2_GLED              ( oCIS2_GLED          ),
    .oCIS2_BLED              ( oCIS2_BLED          ),
    .oCIS2_IRLED             ( oCIS2_IRLED         ),
    .oCIS2_UVLED             ( oCIS2_UVLED         ),
    .SPI_SI                  ( SPI_SI              ),
    .SPI_SCK                 ( SPI_SCK             ),
    .SPI_CSSN                ( SPI_CSSN            ),
    .LP00                    ( LP00                ),
    .LP01                    ( LP01                ),
    .LP10                    ( LP10                ),
    .LP11                    ( LP11                ),
    .LPCLK0                  ( LPCLK0              ),
    .LPCLK1                  ( LPCLK1              ),
    .D0_p                    ( D0_p                ),
    .D0_n                    ( D0_n                ),
    .D1_p                    ( D1_p                ),
    .D1_n                    ( D1_n                ),
    .DCK_p                   ( DCK_p               ),
    .DCK_n                   ( DCK_n               ),

    .oCLK_V72                ( oCLK_V72     [1:0]  ),
    .oLED_R_T                ( oLED_R_T            ),
    .oLED_G_T                ( oLED_G_T            ),
    .oLED_B_T                ( oLED_B_T            ),
    .oLED_IR_T               ( oLED_IR_T           ),
    .SPI_SO                  ( SPI_SO              ),
    .oDVP_PCLK               ( oDVP_PCLK           ),
    .oDVP_D                  ( oDVP_D       [7:0]  ),
    .oDVP_VS                 ( oDVP_VS             ),
    .oDVP_HS                 ( oDVP_HS             ),
    .TP19                    ( TP19                ),
    .TP42                    ( TP42                ),
    .oLED                    ( oLED         [2:0]  )
);

`endif

`ifdef DVP_TEST_MODE

     wire dvp_pix_clk = OSC_50MHz;
     wire dvp_field_valid = FieldValid;
     wire dvp_line_valid = LineValid;
     wire [7:0] dvp_parallel_data = ParallelVideo[7:0];

`else

     wire dvp_pix_clk = oDVP_PCLK;
     wire dvp_field_valid = oDVP_VS;
     wire dvp_line_valid = oDVP_HS;
     wire [7:0] dvp_parallel_data = oDVP_D[7:0];

`endif 

mipi_tx_bridge_top #(
    .VC         ( VC         ),
    .WC         ( WC         ),
    .word_width ( word_width ),
    .DT         ( DT         ),
    .testmode   ( testmode   ),
    .crc16      ( crc16      ),
    .reserved   ( reserved   ),
    .lane_width ( lane_width ))
	
 u_mipi_tx_bridge_top (
    .reset_n  (reset_n),
    .PIXCLK   (dvp_pix_clk),
    .FV       (dvp_field_valid),
    .LV       (dvp_line_valid),
    .PIXDATA  (dvp_parallel_data[7:0]),
	
    .DCK( DCK),

    `ifdef  FPGA_LCMXO2_2000HC
    .FCLK_12_5Mhz            ( FCLK_12_5Mhz),
    .FCLK_100Mhz             ( FCLK_100Mhz),
    .FCLK_83Mhz              ( FCLK_83Mhz),
    `endif

   // .D3( D3),
   // .D2( D2),
    .D1( D1),
    .D0( D0)

    //.LPCLK( LPCLK[1:0]),
    //.LP3( LP3[1:0]),
    //.LP2( LP2[1:0]),
    //.LP1( LP1[1:0]),
    //.LP0( LP0[1:0])

);


endmodule
