module clk_gen
#(parameter CLK_DIV = 1)
(
	input   reset_n,
    input   clock_in,
    output reg   clock_out
);


reg[15:0] clock_counter;


always @ ( posedge clock_in or negedge reset_n)
begin
	if( ~reset_n)
		begin
			clock_counter 		    <= 16'd0;
            clock_out               <=1'b0;
		end
	else 
	    begin
            if (clock_counter < CLK_DIV/2)
                clock_counter <= clock_counter+1;
            else
                begin
                    clock_counter 		    <= 16'd0;
                    clock_out               <= !clock_out;
                end    
        end
end

endmodule                