/** 
 ** compiler directives 
 ** Lattice MachXO2-2000HC
 ** Thomas Wang, 2019-11-19
 **/
`timescale 1 ns / 10 ps

`define index_bit 12
`define OSC_Freq 50000000;  // OSC 50MHz
`define V72_Freq 48000000;  // V72 VPFE 48MHz  
`define DVP_PCLK_Freq 40000000; // DVP PCLK 40MHz
`define debug_pin 9
//`define SysCountLen 32;

/** compiler **/
//`define Code_Reserve
//`define ENABLE_DVP_F429
//`define LINK_V72_P1_DVP
//`define LINK_V72_P2_DVP

/**
 ** Module: Top 
 **/

/** SYSTEM **/
/** Clock **/
//`define ENABLE_PLL
//`define PLL_120_12
//`define ENABLE_OSCH
//`define DVP_40MHz

/** Debug **/
//`define DEBUG_MONITOR
//`define DEBUG_EBR
//`define DEBUG_CAP
//`define DEBUG_CAP1
//`define DEBUG_DVP
//`define DEBUG_DVP1
//`define DEBUG_PASS_V72
//`define LINK_V72_P1_DVP
//`define LINK_V72_P2_DVP

/** HT82V72 VPFE **/
//`define ENABLE_HT82V72_M4
//`define ENABLE_HT82V72_SDP  // read from FIFO, write to SDP

/** Lattice Block RAM **/


/** 
 ** Module: DVP 
 **/
//`define DVP_LINE1
//`define DVP_LINE2
//`define DVP_LINE3
//`define DVP_LINE4
//`define DVP_LINE5
//`define DVP_LINE6
`define ENABLE_DVP_SYNC     // oDVP_D connect to EBR
//`define ENABLE_DVP_SYNC_DATA     /* mark out for F429 test */
//`define ENABLE_DVP_TIME_GEN
//`define ENABLE_DVP_PATTERN


/** 
 ** Module: Others 
 **/
 /** LED control **/
//`define LED_colors  // defined in top.v
`define LED_R
`define LED_G
`define LED_B
`define LED_IR1
`define LED_IR2
`define LED_UV

 /** Communication bus **/
//`define ENABLE_SPI_SLAVE
//`define ENABLE_I2C_SLAVE

//`define ENABLE_BreathLED
//`define ENABLE_COUNTER1
`define ENABLE_COUNTER_16
`define ENABLE_COUNTER_32


//`ifndef    
//`ifdef
//`endif

/** LED-T control
 ** L-PX-2590(CIS#1)-Br,UVr,Rr,IRr,Gr,Wp,Rr,IRp
 ** R-PX-2560(CIS#2)-Br,UVr,Rr,IRr,Gr,Wr,Rr,IRr
 **/

/** End of Compiler directives **/