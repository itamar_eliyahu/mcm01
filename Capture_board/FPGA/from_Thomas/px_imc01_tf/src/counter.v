`timescale 1 ns / 10 ps
`include "compiler_directives.v"


/**
 ** Free Counter16
 **/
//`ifdef ENABLE_COUNTER_16
module COUNTER_16
(
	input wire clock,
	input wire reset,
	output wire [15:0] Q
);	
LT_COUNTER_16 u_LT_COUNTER_F2B
(
	.Clock(clock),
	.Clk_En(1'b1), 
	.Aclr(reset), 
	.Q(Q)
);
endmodule	// COUNTER2
//`endif


/**
 ** Free Counter32
 **/
//`ifdef ENABLE_COUNTER_32
module COUNTER_32
(
	input wire clock,
	input wire reset,
	output wire [31:0] Q
);
LT_COUNTER_32 u_LT_COUNTER_32
(
	.Clock(clock),
	.Clk_En(1'b1), 
	.Aclr(reset), 
	.Q(Q)
);
endmodule
//`endif

/*
module INDEX_DVP
#(parameter CntSize = 16)
(
	input   wire clock,
	output  reg	[CntSize-1:0] index_dvp,
	input	wire rst
);
	always @(posedge clock) 
	if(rst)
	begin
		index_dvp <= 0; 
	end
	else
	begin 
		index_dvp <= index_dvp + 1'b1;
	end

endmodule
*/
// INDEX_DVP

/*
module INDEX_AFE
#(parameter CntSize = 16)
(
	input   wire clock,
	output  reg	[CntSize-1:0] index_afe,
	input	wire rst
);
	always @(posedge clock) 
	if(rst)
	begin
		index_afe <= 0; 
	end
	else
	begin 
		index_afe <= index_afe + 1'b1;
	end
endmodule
*/
// INDEX_AFE

