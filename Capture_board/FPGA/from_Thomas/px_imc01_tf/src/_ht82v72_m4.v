/** 
 ** HT82V72 & M4
 ** VPFE I/F : HT82V72
 ** SPI I/F : M4
 ** FIFO 16b-to-8bit x2
 ** FSM functions
 ** This module provides the state machine 
 ** for HT82V72 2chips and SPI collect
 ** HT82V72 copy datas to FIFO when HSYNC assert
 ** Move FIFO datas to EBR when HSYNC de-assert
 **
 ** -- parameterized module instance --
 ** Ref: Memory Usage Guide for MachXO2 Devices.pdf 
 ** Thomas Wang, 2019-12-04
 **
 **/

`include "compiler_directives.v"

module HT82V72_M4
#(parameter CIS_size = 1440,
  parameter LED_colors = 8,
  parameter M4_sensors = 38
)
(
	input wire reset_n,
	input wire clock,	// clock for CAP

`ifdef DEBUG_CAP
	/** debug port **/
	output [`debug_pin-1:0] debug_o,
`endif

/** HT82V72 #1 **/
	input wire vsync1,
	input wire hsync1,
	input wire pclk1,
	input wire [15:0] data_i1,	// HT82V72 16bit output

/** HT82V72 #2 **/
	input wire vsync2,
	input wire hsync2,
	input wire pclk2,
	input wire [15:0] data_i2,	// HT82V72 16bit output

	/** output port **/
	output wire [11:0] index_o,	// 12bit
	output wire [7:0] data_o,	// cap_data
	output wire pclk_o,		// cap_pclk
	
	output wire write_en
//	output wire frame_done
)/* synthesis syn_force_pads=1 syn_noprune = 1 */;

	
/**
 **	System Registors
 **/
	reg [11:0] index_u1 = 12'h000; // addr, 12bit
	reg [11:0] index_u2 = 12'h000;
	reg fifo_rd_u1 = 1'b0;
	reg fifo_rd_u2 = 1'b0;
	reg fifo_reset = 1'b0;
	reg dtx_en     = 1'b0;


/**
 **	FIFO for U1
 **/
wire sync_wr1 = vsync1 & hsync1;
wire fifo_clk1 = hsync1 & ~pclk1; 
wire [7:0] fifo_o1;
reg [15:0] fifo_i1; // data, 16bit 
//wire fifo_emp1;
//wire fifo_aemp1;
//wire fifo_ful1;
//wire fifo_aful1;

//	assign #1	fifo_i1[15:8] = (sync_wr1 == 1'b1)? data_i1[7:0]:16'h0000;
//	assign #1	fifo_i1[7:0] = (sync_wr1 == 1'b1)? data_i1[15:8]:16'h0000;

	always @(negedge pclk1)
	begin
		fifo_i1[15:8] <= data_i1[7:0];
		fifo_i1[7:0]  <= data_i1[15:8];
	end

	LT_FIFO u_FIFO_U1
	(
		.Data(fifo_i1),
		.WrClock(fifo_clk1),
		.RdClock(clock),
		.WrEn(sync_wr1),
		.RdEn(fifo_rd_u1),
		.Reset(fifo_reset),
		.RPReset(fifo_reset),		
		.Q(fifo_o1),
		.Empty(), //fifo_emp1),
		.Full(), //fifo_ful1),
		.AlmostEmpty(), //fifo_aemp1), 
		.AlmostFull() //fifo_aful1)
	);

/**
 **	FIFO for U2
 **/
wire sync_wr2 = vsync2 & hsync2;
wire fifo_clk2 = hsync2 & ~pclk2;
wire [7:0] fifo_o2;
reg [15:0] fifo_i2;
//wire fifo_emp2;
//wire fifo_aemp2;
//wire fifo_ful2;
//wire fifo_aful2;

//	assign #1	fifo_i2[15:8] = (sync_wr2 == 1'b1)?data_i2[7:0]:16'h0000;
//	assign #1	fifo_i2[7:0] = (sync_wr2 == 1'b1)?data_i2[15:8]:16'h0000;

	always @(negedge pclk2)
	begin
		fifo_i2[15:8] <= data_i2[7:0];
		fifo_i2[7:0]  <= data_i2[15:8];
	end

	LT_FIFO u_FIFO_U2
	(
		.Data(fifo_i2),
		.WrClock(fifo_clk2),
		.RdClock(clock),
		.WrEn(sync_wr2),
		.RdEn(fifo_rd_u2),
		.Reset(fifo_reset),
		.RPReset(fifo_reset),
		.Q(fifo_o2),
		.Empty(), //fifo_emp2),
		.Full(), //fifo_ful2),
		.AlmostEmpty(), //fifo_aemp2), 
		.AlmostFull() //fifo_aful2)
	);


/**
 **	Buffer for M4
 **/
//	parameter M4_sensors = 38;
	reg [7:0] index_m4 = 8'h00;	// reg for 38byte
	reg [7:0] ram_m4 [0:40] /* synthesis syn_ramstyle="registers" */; // "block_ram"
	reg m4_rd = 1'b0;

	wire [11:0] index_f2b = 12'h0000;
	reg [15:0] index_cnt = 16'h0000;

/**
 ** edge detector
 **/
	wire [3:0] clock_div_cap;
	wire posedge_hs;
	wire negedge_hs;

/** 
 ** Clock div n for edge capture
 **/
clock_div16 u_clock_div16_dvp_vs
(
    .clock(clock), 
    .reset_n(reset_n), 
    .clock_div16(clock_div_cap)
);


/** 
 ** Edge detect to hsync high edge
 **/
	pos_edge_det u_poxs_edge_hs
	(
		.reset_n(reset_n),
		.sig(hsync1|hsync2),
		.clock(clock_div_cap[1]),
		.pe(posedge_hs)
	); 

/** 
 **  Edge detect to hsync low edge
 **/
	pos_edge_det u_negedge_hs
	(
		.reset_n(reset_n),
		.sig(~(hsync1|hsync2)),
		.clock(clock_div_cap[1]),
		.pe(negedge_hs)
	); 


/**
 ** FSM for Capture
 ** Ref: Designing Safe Verilog State Machines with Synplify.pdf
 **/
//	localparam CAP_VIDLE  = 8'b0000_0001;
	localparam CAP_DTX    = 8'b0000_0001;
	localparam CAP_HIDLE  = 8'b0000_0010;
	localparam CAP_FIFO   = 8'b0000_0100;
//	localparam CAP_INIT   = 8'b0000_1000;
//	localparam CAP_TX_U1  = 8'b0001_0000;
//	localparam CAP_TX_U2  = 8'b0010_0000;
//	localparam CAP_TX_M4  = 8'b0100_0000;
//	localparam CAP_RESET  = 8'b1000_0000;

	reg [7:0] cap_state;
//	reg [7:0] state_next;

	always @(posedge clock) // or negedge reset_n)
	begin:FSM
		if(~reset_n)
			cap_state <= #1 CAP_HIDLE;
		else
		case(cap_state)
	/*
			CAP_VIDLE:
			if ((vsync1 | vsync2) == 1'b1)
				cap_state <= #1 CAP_HIDLE;
			else
				cap_state <= #1 CAP_VIDLE;
	*/
			CAP_HIDLE:
			if (posedge_hs)
				cap_state <= #1 CAP_FIFO;
			else
				cap_state <= #1 CAP_HIDLE;

			CAP_FIFO:
			if (negedge_hs)
	//			cap_state <= #1 CAP_INIT;
				cap_state <= #1 CAP_DTX;
			else
				cap_state <= #1 CAP_FIFO;

	/*
			CAP_INIT:
			if (index_cnt > 16'd10) 
				cap_state <= #1 CAP_TX_U1;
			else
				cap_state <= #1 CAP_INIT;

			CAP_TX_U1:
			if (index_cnt > 16'd1450) 
				cap_state <= #1 CAP_TX_U2;
			else
				cap_state <= #1 CAP_TX_U1;

			CAP_TX_U2:
			if (index_cnt > 16'd2890) 
				cap_state <= #1 CAP_TX_M4;
			else
				cap_state <= #1 CAP_TX_U2;

			CAP_TX_M4:
			if (index_cnt > 16'd2928) 
				cap_state <= #1 CAP_RESET;
			else
				cap_state <= #1 CAP_TX_M4;

			
			CAP_RESET:
			if (index_cnt > 16'd2938) 
				cap_state <= #1 CAP_VIDLE;
			else
				cap_state <= #1 CAP_RESET;
	*/

			CAP_DTX:
			if (index_f2b >= 16'd2940)		
				cap_state <= #1 CAP_HIDLE;
			else
				cap_state <= #1 CAP_DTX;

			default:
				cap_state <= #1 CAP_HIDLE;

		endcase
	end


/** index_cnt **/
	always @(posedge clock or negedge reset_n)
	if ((~reset_n)|(cap_state[7:0] == CAP_FIFO))  // ?????????
		index_cnt  <= #1 16'h0000;
	else
	//if (index_cnt < 12'd3000)	
		index_cnt  <= #1 index_cnt + 16'h0001;


/** 
 **index_f2b
 **/
	wire cnt16_reset;
	wire [15:0] cnt16_f2b;

	assign cnt16_reset = (cap_state[7:0] == CAP_FIFO)? 1'b1: 1'b0;
	assign index_f2b[11:0] = cnt16_f2b[11:0];

	LT_COUNTER_16 u_LT_COUNTER_16
	(
		.Clock(clock),
		.Clk_En(1'b1), 
		.Aclr(cnt16_reset), 
		.Q(cnt16_f2b)
	);

	always @(posedge clock)
	begin
		if (cap_state[7:0] == CAP_DTX)
		begin
			if (index_f2b < 12'd1440)	// tx, U1
			begin
				dtx_en     <= #1 1'b1;
				fifo_rd_u1 <= #1 1'b1;
				fifo_rd_u2 <= #1 1'b0;
				m4_rd      <= #1 1'b0;
				fifo_reset <= #1 1'b0;
			end
			else
			if (index_f2b < 12'd2880)	// tx, U2
			begin
				dtx_en     <= #1 1'b1;
				fifo_rd_u1 <= #1 1'b0;
				fifo_rd_u2 <= #1 1'b1;
				m4_rd      <= #1 1'b0;
				fifo_reset <= #1 1'b0;
			end
			else
			if (index_f2b < 12'd2918)	 // tx, M4 
			begin
				dtx_en     <= #1 1'b1;
				fifo_rd_u1 <= #1 1'b0;
				fifo_rd_u2 <= #1 1'b0;
				m4_rd      <= #1 1'b1;
				fifo_reset <= #1 1'b0;
			end
			else
			if (index_f2b < 12'd2928)	// fifo reset assert
			begin
				dtx_en     <= #1 1'b0;
				fifo_rd_u1 <= #1 1'b0;
				fifo_rd_u2 <= #1 1'b0;
				m4_rd      <= #1 1'b0;
				fifo_reset <= #1 1'b1;
			end
			else
			if (index_f2b < 12'd2938)	// fifo reset de-assert
			begin
				dtx_en     <= #1 1'b0;
				fifo_rd_u1 <= #1 1'b0;
				fifo_rd_u2 <= #1 1'b0;
				m4_rd      <= #1 1'b0;
				fifo_reset <= #1 1'b0;
			end
		end
	end

/*
	assign #1 fifo_rd_u1 = (cap_state == CAP_TX_U1)? 1'b1: 1'b0;
	assign #1 fifo_rd_u2 = (cap_state == CAP_TX_U2)? 1'b1: 1'b0;
	assign #1 m4_rd      = (cap_state == CAP_TX_M4)? 1'b1: 1'b0;
	assign #1 fifo_reset = (cap_state == CAP_RESET)? 1'b1: 1'b0;
*/
//	assign #1 write_en   = (fifo_rd_u1 | fifo_rd_u2 | m4_rd)? 1'b1: 1'b0;
	assign #1 write_en   = (dtx_en == 1'b1)? 1'b1: 1'b0;
//	assign #1 write_en   = dtx_en;
	assign #1 index_o[11:0] = (dtx_en == 1'b1)? index_f2b[11:0]: 12'h000;
	assign #1 pclk_o = clock; // (dtx_en == 1'b1)? clock: 1'b0;
	assign #1 data_o[7:0] = (fifo_rd_u1)? fifo_o1[7:0]: 
						(fifo_rd_u2)? fifo_o2[7:0]:
						(m4_rd)? ram_m4[index_m4]: 8'h00;


`ifdef DEBUG_CAP
//	assign #1 debug_o[12] = index_f2b[2]; 	// TP46
//	assign #1 debug_o[11] = index_f2b[2]; 	// TP42
//	assign #1 debug_o[10] = index_f2b[1]; 	// TP31 
//	assign #1 debug_o[9]  = index_f2b[0];	// TP19
	assign #1 debug_o[8]  = data_o[2];		// R115 (Left)

	assign #1 debug_o[7]  = data_o[1];	// R111
	assign #1 debug_o[6]  = data_o[0];	// R109
	assign #1 debug_o[5]  = fifo_o1[1];	// R106
	assign #1 debug_o[4]  = fifo_o1[0];	// R105

	assign #1 debug_o[3]  = fifo_rd_u2;	// R102 (right)
	assign #1 debug_o[2]  = fifo_rd_u1;	// LED6
	assign #1 debug_o[1]  = write_en;	// LED5
	assign #1 debug_o[0]  = pclk_o;	// LED4
`endif

endmodule	// HT82V72_M4
