/**
 ** DVP output
 ** Thomas Wang, 2019-12-24
 ** FSM functions
 **/

`include "compiler_directives.v"

module DVP_OUTPUT
#(parameter DVP_pixels = 2918,
  parameter LED_colors = 8,
  parameter M4_sensors = 38
)
(
	input wire reset_n,
    input wire clock,
//	input wire clock_sys,

    input wire vsync_i,
    input wire hsync_i,

`ifdef DEBUG_DVP
	/** debug **/
	output wire [`debug_pin-1:0] debug_o,
`endif

	output wire [11:0] index_o,
	output wire vsync_o,
	output wire hsync_o,
	output wire pclk_o
)/* synthesis syn_force_pads=1 syn_noprune = 1 */;

//	reg [4:0]  dvp_state_c;
//	reg [4:0]  dvp_state_n;
//	wire [15:0] dvp_counter;
//	reg [3:0]  line_index;
//	reg dvp_tx_en;
//	reg dvp_cnt_reset;
//	reg dvp_cnt_en;
//	reg [15:0] dvp_index;

//	parameter DVP_Datas	= 12'b1011_0110_0110;

// State Machine Definition
	localparam VS_IDLE      = 8'b0000_0001;
	localparam VS_HOLD      = 8'b0000_0010;
	localparam VS_ASSERT    = 8'b0000_0100;
	localparam VS_KEEP      = 8'b0000_1000;
	localparam HS_IDLE      = 8'b0001_0000;
	localparam HS_ASSERT    = 8'b0010_0000;
	localparam HS_KEEP      = 8'b0100_0000;

	reg [7:0] vs_state ; /* syn_encoding = "onehot" */
	reg [15:0] vs_hold_cnt;
	wire vs_edge;
	reg vs_act;
	reg [7:0] hs_state ; /* syn_encoding = "onehot" */
	reg [15:0] hs_counter;
	wire hs_edge;
	reg hs_act;
	wire [3:0] clock_div_dvp;

/** 
 ** Clock div n for edge capture
 **/
	clock_div16 u_clock_div16_vs
	(
		.clock(clock), 
		.reset(!reset_n), 
		.clock_div16(clock_div_dvp)
	);

/*
	posedge_det u_dvp_edge_vs 
	(
		.reset_n(reset_n),
		.sig(vsync_i),
		.clock(clock_div_dvp[1]),
		.pe(vs_edge)
	); 
*/
	edge_detector u_dvp_edge_vs
	(
		.clock(clock), 
		.reset(~reset_n), 
		.level(vsync_i), 
		.Mealy_tick(), 
		.Moore_tick(vs_edge)
	);


/*
	posedge_det u_dvp_edge_hs 
	(
		.reset_n(reset_n),
		.sig(hsync_i),
		.clock(clock_div_dvp[1]),
		.pe(hs_edge)
	); 
*/

	edge_detector u_dvp_edge_hs
	(
		.clock(clock), 
		.reset(~reset_n), 
		.level(hsync_i), 
		.Mealy_tick(), 
		.Moore_tick(hs_edge)
	);


/**
 ** FSM for DVP
 **/
	always @(posedge clock) // or posedge vs_edge)
	begin:FSM
		if (~reset_n)
		begin
			vs_state <= #1 VS_IDLE;
			vs_hold_cnt   <= #1 16'h0000;
		end
		else
		case(vs_state)
		VS_IDLE:
		begin
			if (vs_edge == 1'b1)
				vs_state <= #1 VS_HOLD;
			else
			begin
				vs_state <= #1 VS_IDLE;
				vs_hold_cnt <= #1 16'h0000;
			end
		end

		VS_HOLD:
		begin
			vs_hold_cnt <= #1 vs_hold_cnt + 16'h0001;

			if (vs_hold_cnt >= 16'd2000)		
				vs_state <= #1 VS_ASSERT;
			else
				vs_state <= #1 VS_HOLD;

			vs_act <= 1'b0;
		end

		VS_ASSERT:
		begin
			vs_hold_cnt <= #1 vs_hold_cnt + 16'h0001;

			if (vs_hold_cnt >= 16'd2500)
			begin
				vs_state <= #1 VS_KEEP;
//				vs_act <= 1'b0;
			end
			else
			begin
				vs_state <= #1 VS_ASSERT;
//				vs_act <= 1'b1;
			end

			vs_act <= 1'b1;
		end

		VS_KEEP:
		begin
			if (vsync_i == 1'b0)
				vs_state <= #1 VS_IDLE;
			else
				vs_state <= #1 VS_KEEP;
				
			vs_act <= 1'b0;
		end

		default:
			vs_state <= #1 VS_IDLE;

		endcase	
	end


	always @(negedge clock) // or negedge reset_n) // or posedge hs_edge)
	begin
		if (~reset_n)
		begin
			hs_counter <= #1 16'h0000;
			hs_state   <= #1 HS_IDLE;
//			hs_flag    <= #1 1'b0;
		end
		else
		case(hs_state)
			HS_IDLE:
			begin
	//			hs_flag <= #1 1'b0; 
				hs_counter <= #1 16'h0000;
				if (hs_edge == 1'b1)
					hs_state <= #1 HS_ASSERT;
				else
					hs_state <= #1 HS_IDLE;
			end

			HS_ASSERT:
			begin
				hs_counter <= #1 hs_counter + 16'h0001;
				if (hs_counter >= 16'd2918) //  NG: DVP_pixels-1)
				begin
					hs_state <= #1 HS_KEEP;
					hs_act <= 1'b0;
				end
				else
				begin
					hs_state <= #1 HS_ASSERT;
					hs_act <= 1'b1;
				end
			end

			HS_KEEP:
			begin
				if (hsync_i == 1'b0)
					hs_state <= #1 HS_IDLE;
				else
					hs_state <= #1 HS_KEEP;			
			end

			default:
				hs_state <= #1 HS_IDLE;

		endcase	
	end

//	assign #1 vsync_o = (vs_state[7:0] == VS_ASSERT)? 1'b1: 1'b0;
//	assign #1 hsync_o = (hs_state[7:0] == HS_ASSERT)? 1'b1: 1'b0;
	assign #1 vsync_o = vs_act;
	assign #1 hsync_o = hs_act;
//	assign #1 index_o[11:0] = (hs_state[7:0] == HS_ASSERT)? hs_counter[11:0]: 12'h000;
	assign #1 index_o[11:0] = (hs_act)? hs_counter[11:0]: 12'h000;
	assign #1 pclk_o = clock; // (hs_state[7:0] == HS_ASSERT)? clock: 1'b0;

/*
	assign #1 vsync_o = (vs_state[2] == 1'b1)? 1'b1: 1'b0;
	assign #1 hsync_o = (hs_state[1] == 1'b1)? 1'b1: 1'b0;
	assign #1 index_o[11:0] = (hs_state[1] == 1'b1)? hs_counter[11:0]: 12'b0;
	assign #1 pclk_o = (hs_state[1] == 1'b1)? clock: 1'b0;
*/

// generate "pattern"
`ifdef ENABLE_DVP_PATTERN

	reg [7:0] data;

	always @(posedge clock)
	begin
		data[7:0] <= #1 dvp_index[7:0]; 
	end
`endif


/** debug port**/	
`ifdef DEBUG_DVP
//	assign #1 debug_o[12] = index_o[12]; // TP46
//	assign #1 debug_o[11] = index_o[11]; // TP42
//	assign #1 debug_o[10] = index_o[10]; // TP31
//	assign #1 debug_o[9]  = index_o[9];	// TP19
	assign #1 debug_o[8]  = index_o[8];	// R115 (left)

	assign #1 debug_o[7]  = index_o[7];	// R111
	assign #1 debug_o[6]  = index_o[6];	// R109
	assign #1 debug_o[5]  = index_o[5];	// R106
	assign #1 debug_o[4]  = index_o[4];	// R105

	assign #1 debug_o[3]  = hs_edge;	// R105 (right)
	assign #1 debug_o[2]  = vs_edge;	// LED6
	assign #1 debug_o[1]  = hsync_o;	// LED5
	assign #1 debug_o[0]  = vsync_o;	// LED4
`endif

endmodule	// dvp_output
