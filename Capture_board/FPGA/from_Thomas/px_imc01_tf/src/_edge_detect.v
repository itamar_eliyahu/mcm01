/**
 ** Rising edge detector
 ** https://verilogguide.readthedocs.io/en/latest/verilog/fsm.html
 **/
 
// edgeDetector.v
// Moore and Mealy Implementation

//`timescale 1 ns / 10 ps
`define PosEdgeDet1
`ifdef  PosEdgeDet1

module edge_detector
(
    input wire clock, reset, 
    input wire level, 
    output reg Mealy_tick, Moore_tick
)/* synthesis syn_force_pads=1 syn_noprune = 1 */;

localparam  // 2 states are required for Mealy
    zeroMealy = 1'b0,
    oneMealy =  1'b1;
    
localparam [2:0] // 3 states are required for Moore
    zeroMoore = 3'b000,
    edgeMoore = 3'b010, 
    oneMoore =  3'b100;

reg stateMealy_reg, stateMealy_next; 
reg [1:0] stateMoore_reg; 
reg [1:0] stateMoore_next;

always @(posedge clock or posedge reset)
begin
    if(reset) // go to state zero if rese
        begin
        stateMealy_reg <= #1 zeroMealy;
        stateMoore_reg <= #1 zeroMoore;
        end
    else // otherwise update the states
        begin
        stateMealy_reg <= #1 stateMealy_next;
        stateMoore_reg <= #1 stateMoore_next;
        end
end

// Mealy Design 
always @(stateMealy_reg or level)
begin
    // store current state as next
    stateMealy_next = stateMealy_reg; // required: when no case statement is satisfied
    
    Mealy_tick = 1'b0; // set tick to zero (so that 'tick = 1' is available for 1 cycle only)
    case(stateMealy_reg)
        zeroMealy: // set 'tick = 1' if state = zero and level = '1'
            if(level)  
                begin // if level is 1, then go to state one,
                    stateMealy_next = oneMealy; // otherwise remain in same state.
                    Mealy_tick = 1'b1;
                end
        oneMealy: 
            if(~level) // if level is 0, then go to zero state,
                stateMealy_next = zeroMealy; // otherwise remain in one state.
    endcase
end

// Moore Design 
always @(stateMoore_reg or level)
begin
    // store current state as next
    stateMoore_next = stateMoore_reg; // required: when no case statement is satisfied
    
    Moore_tick = 1'b0; // set tick to zero (so that 'tick = 1' is available for 1 cycle only)
    case(stateMoore_reg)
        zeroMoore: // if state is zero,
            if(level) // and level is 1
                stateMoore_next = edgeMoore; // then go to state edge.
        edgeMoore:
            begin
                Moore_tick = 1'b1; // set the tick to 1.
                if(level) // if level is 1, 
                    stateMoore_next = oneMoore; // go to state one,
                else    
                    stateMoore_next = zeroMoore; // else go to state zero.
            end
        oneMoore: 
            if(~level) // if level is 0,
                stateMoore_next = zeroMoore; // then go to state zero.      
    endcase
end
endmodule
`endif


//`define PosEdgeDet2
`ifdef  PosEdgeDet2
/**
 ** https://www.chipverify.com/verilog/verilog-positive-edge-detector
**/

module pos_edge_det ( input sig,            // Input signal for which positive edge has to be detected
                      input clk,            // Input signal for clock
                      output pe);           // Output signal that gives a pulse when a positive edge occurs
 
    reg   sig_dly;                          // Internal signal to store the delayed version of signal
 
    // This always block ensures that sig_dly is exactly 1 clock behind sig
  always @ (posedge clk) begin
    sig_dly <= sig;
  end
 
    // Combinational logic where sig is AND with delayed, inverted version of sig
    // Assign statement assigns the evaluated expression in the RHS to the internal net pe
  assign pe = sig & ~sig_dly;            
endmodule 
`endif


/**
 ** https://rtldigitaldesign.blogspot.com/2014/06/pos-n-neg-edge-detector.html
 **/
`define PosEdgeDet3
`ifdef  PosEdgeDet3
module pos_edge_det 
(
//    clock, reset_n, sig, pe);
    input wire clock,
    input wire reset_n,
    input wire sig,
    output wire pe
);
reg d_ff;

always @(posedge clock or negedge reset_n) 
begin
    if(~reset_n)
        d_ff <= 1'b0;
    else
        d_ff <= sig;
end


// Positive edge detection
assign pe = sig && (d_ff ^ sig);

// Negative edge detection
// assign pe = (~sig) && (d_ff ^ sig);

endmodule
`endif


/** 
 ** Clock div n for edge capture
 **/
module clock_div16
(
    input wire clock, 
    input wire reset_n, 
    output wire [3:0] clock_div16
);
reg [3:0] counter;
always @(posedge clock or negedge reset_n)
begin
	if (~reset_n)
		counter <= #1 4'h0;
	else
		counter <= #1 counter + 4'h1;
end
assign #1 clock_div16[3:0] = counter[3:0];

endmodule